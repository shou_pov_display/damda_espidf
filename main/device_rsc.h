
//###########################################################################
//
// FILE:   device_rsc.h
//
// TITLE:  DAMDA Device Resource Data Define File and Global !!!
//
//###########################################################################



#define DEVICE_MANUFACTURER  		"KONO"
#define DEVICE_MODEL  				"SHOU"
//#define DEVICE_SERIAL  				"16003902"
#define DEVICE_TYPE  				"Digital_Sign_Display"

// #define DEVICE_MANUFACTURER 		"WINIAELECTRONICS"
// #define DEVICE_MODEL 				"WN_ULTFREEZER"
// #define DEVICE_TYPE 				"FREEZER"

#define RSC_OID 		"oid"       // object id
#define RSC_RID 		"rid"       // resource i
#define RSC_COUNT 		"count"     // instance 갯 수
#define RSC_TYPE 		"type"      // data type : I (int), B (boolean), S (string), T (time)
#define RSC_RANGE 		"range"     // range(값의 범위)

/* 윈도우 동작 O*/
/* 
    char *deviceRscData = "[{\"oid\":\"8000\",\"rid\":\"1630\",\"count\":\"1\",\"type\":\"S\",\"range\":\"\"},{\"oid\":\"8000\",\"rid\":\"5200\",\"count\":\"1\",\"type\":\"S\",\"range\":\"\"},{\"oid\":\"1403\",\"rid\":\"4613\",\"count\":\"1\",\"type\":\"T\",\"range\":\"\"},{\"oid\":\"1403\",\"rid\":\"4616\",\"count\":\"1\",\"type\":\"T\",\"range\":\"\"},{\"oid\":\"2606\",\"rid\":\"5001\",\"count\":\"1\",\"type\":\"S\",\"range\":\"\"},{\"oid\":\"100\",\"rid\":\"0\",\"count\":\"1\",\"type\":\"I\",\"range\":\"\"},{\"oid\":\"8000\",\"rid\":\"4614\",\"count\":\"1\",\"type\":\"S\",\"range\":\"\"},{\"oid\":\"2606\",\"rid\":\"5005\",\"count\":\"1\",\"type\":\"S\",\"range\":\"\"},{\"oid\":\"4800\",\"rid\":\"4802\",\"count\":\"1\",\"type\":\"F\",\"range\":\"0~100\"},{\"oid\":\"2404\",\"rid\":\"2409\",\"count\":\"1\",\"type\":\"I\",\"range\":\"0~100\"},{\"oid\":\"1004\",\"rid\":\"2005\",\"count\":\"1\",\"type\":\"I\",\"range\":\"\"},{\"oid\":\"4805\",\"rid\":\"4018\",\"count\":\"1\",\"type\":\"I\",\"range\":\"\"},{\"oid\":\"4001\",\"rid\":\"4016\",\"count\":\"1\",\"type\":\"B\",\"range\":\"\"}]";
*/    


/* 
    data type : I (int), B (boolean), S (string), T (time)
    instance 갯 수 : 2, 0 :  전체, 1 : 실내온도 1개
    인스턴스 갯 수가 2인 경우 0과 1을 나타낸다. 0은 전체를 1은 실내 온도를 나타낸다.
    인스턴스 갯 수가 3인 경우 0과 1, 2를 나타낸다. 0은 전체를 1은 실내 온도1, 2는 샐내 온도 2를 나타낸다.
*/

/* 가독성 높게 변경 */
char *deviceRscData = 
    "["
        "{"
            "\"oid\":\"8000\","
            "\"rid\":\"1630\","
            "\"count\":\"1\","
            "\"type\":\"S\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"8000\","
            "\"rid\":\"5200\","
            "\"count\":\"1\","
            "\"type\":\"S\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"1403\","
            "\"rid\":\"4613\","
            "\"count\":\"1\","
            "\"type\":\"T\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"1403\","
            "\"rid\":\"4616\","
            "\"count\":\"1\","
            "\"type\":\"T\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"2606\","
            "\"rid\":\"5001\","
            "\"count\":\"1\","
            "\"type\":\"S\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"100\","
            "\"rid\":\"0\","
            "\"count\":\"1\","
            "\"type\":\"I\","
            "\"range\":\"0~100\""
        "},"
        "{"
            "\"oid\":\"8000\","
            "\"rid\":\"4614\","
            "\"count\":\"1\","
            "\"type\":\"S\","
            "\"range\":\"\""
        "},"
        // "{"
        //     "\"oid\":\"2606\","
        //     "\"rid\":\"5005\","
        //     "\"count\":\"1\","
        //     "\"type\":\"S\","
        //     "\"range\":\"0~100\""
        // "},"
        "{"
            "\"oid\":\"4800\","
            "\"rid\":\"4802\","
            "\"count\":\"1\","
            "\"type\":\"I\","
            "\"range\":\"-100~100\""
        "},"
        "{"
            "\"oid\":\"2404\","
            "\"rid\":\"2409\","
            "\"count\":\"1\","
            "\"type\":\"I\","
            "\"range\":\"0~100\""
        "},"
        "{"
            "\"oid\":\"1004\","
            "\"rid\":\"2005\","
            "\"count\":\"1\","
            "\"type\":\"I\","
            "\"range\":\"\""
        "},"
        "{"
            "\"oid\":\"4805\","
            "\"rid\":\"4018\","
            "\"count\":\"1\","
            "\"type\":\"I\","
            "\"range\":\"0~100\""
        "},"
        "{"
            "\"oid\":\"4001\","
            "\"rid\":\"4016\","
            "\"count\":\"1\","
            "\"type\":\"B\","
            "\"range\":\"0~1\""
        "}"
    "]";



/*
    NOTI_PARAMS_TOTAL = 각 줄의 RSC(1) X INTANCE 카운트
    총 RSC의 10 줄이고, 각 INTANCE 카운트가 2라면 = 20개
    총 RSC의 10 줄이고, 5개의 INTANCE 카운트가 1이고, 5개는 3이라면 = 25개
*/

#define NOTI_PARAMS_TOTAL 		12
