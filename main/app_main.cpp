#include "device_rsc.h"
#include "damda_ESP32_Project.h"
#include <string>
#include <stdio.h>
#include <SensirionI2CSen5x.h>
#include "esp_http_client.h"


#include <ESPmDNS.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/api.h"
#include "lwip/sockets.h"


/* --Resource codes-- */

/*
Ex) 전원(Power Source)의 리소스 URI : 4001/1/4016	
* 리소스 URI : 객체ID/인스턴스개수/리소스ID	
*/

#ifdef SHOU
    // #define ERROR_CODE                 "/100/1/0"
    // #define IMG_MESSAGE_ID             "/8000/1/4614"
    // #define IMG_MESSAGE_URL            "/2606/1/5005"
    // #define AMBIENT_TEMP               "/4800/1/4802"
    // #define AMBIENT_HUMIDITY           "/2404/1/2409"
    // #define AMBIENT_PM10               "/1004/1/2005"
    // #define AMBIENT_VOC                "/4805/1/4018"
    // #define LED_POWER                  "/4001/1/4016"

    char ERROR_CODE[] = "/100/1/0";
    char IMG_MESSAGE_ID[] = "/8000/1/4614";
    char IMG_MESSAGE_URL[] = "/2606/1/5001";
    char AMBIENT_TEMP[] = "/4800/1/4802";
    char AMBIENT_HUMIDITY[] = "/2404/1/2409";
    char AMBIENT_PM10[] = "/1004/1/2005";
    char AMBIENT_VOC[] = "/4805/1/4018";
    char LED_POWER[] = "/4001/1/4016";
            
    char error_value[100] = "0";
    char img_message_id_value[100] = "1000";
    char img_message_url_value[100] = "https://kocoafab.cc/data/140904110133.jpg";
    char ambient_temp_value[100] = "100";
    char ambient_humidity_value[100] = "20";
    char ambient_pm10_value[100] = "40";
    char ambient_voc_value[100] = "10";
    char led_power_value[100] = "0";
#endif



#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)


/* ---device Infomation--- */
uint8_t eth_mac[6];
char DEVICE_ETH_MAC             [20];
char DEVICE_FIRMWAREVERSION             [10];


char wifi_ssid[100];
char wifi_pass[100];
char device_type_name[100];
bool mqtt_start_flag = false;

flash_info_t flash_info;


char ota_url[200];
char ota_version[10];
char* time_execute;

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

static int s_retry_num = 0;

nvs_handle_t nvs;

tcpip_adapter_ip_info_t ipInfo;

typedef uint64_t UINT64;

char* server_addr_buffer;
char* server_port_buffer;
char* ap_ssid_buffer;
char* ap_password_buffer;
char* auth_key_buffer;

char* ap_ssid_buffer_b;
char* ap_password_buffer_b;

unsigned int _mil_sec = 0;
unsigned int _mil_sec_cnt = 0;
unsigned int noti_mode = 0;

int ucnotify_flag = 0;
char message_print_flag = 0;
char now_time[16] = "";

uint8_t base_mac_addr[6] = {0};	
static httpd_handle_t server = NULL;

int ap_state = 0;

bool ip_get_flag ;

bool init_sntp = false;
bool isForcedWifiStop = false;

//time_t obtain_time(void);
//void OTA_upgrade_fail(void);

void Notify_Func();

void read_sensor_data(void);
static const char *TAG = "APP_MAIN";

bool busy_flag = false;
//=====================================================================================================

int esp_compare_bufferData();
void esp_nvs_set_backupData();
wifi_config_t esp_nvs_get_backupData();
char * esp_nvs_load_value_if_exist(nvs_handle_t nvs, const char* key);

void test_init();
void print_receiveData(ResponseMessageInfo* responseInfo);
void send_url_to_receiver(const char* url) ;
int getCntSplit(char* msg, char* split){
    int i=0;
    char *currPoint=NULL;
    char *prevPoint=msg;
    do{
        currPoint=strstr(prevPoint, split);
        printf("split currPoint : %s\n", currPoint==NULL?"NULL":currPoint);
        if(currPoint!=NULL){
            prevPoint=currPoint+strlen(split);
        }
    } while(currPoint!=NULL && ++i);
    if(i>0) i++;

    return i;
}

int split(char* msg, char* split, char*** result){
    int charCount=3;
    int totalCount=0;
    char *prevPoint=msg;
    char *currPoint=NULL;
    char **array2d=NULL;
    int splitCount = getCntSplit(msg, split);

    array2d=(char**)malloc(sizeof(char*)*(splitCount));
    for(int i=0; i < splitCount; i++){
        currPoint=strstr(prevPoint, split);
        printf("split currPoint : %s\n", currPoint==NULL?"NULL":currPoint);
        if(i<(splitCount-1)){
            if(currPoint!=NULL){
                totalCount=currPoint-msg;
                if(prevPoint==msg) charCount=totalCount;
                else charCount=currPoint-prevPoint;                
                array2d[i]=(char*)malloc(sizeof(char)*30);
                strncpy(array2d[i], prevPoint, charCount);
                
                array2d[i][charCount]='\0';
                prevPoint=currPoint+strlen(split);
            }
        } else {
            charCount=strlen(msg)-totalCount; 
            array2d[i]=(char*)malloc(sizeof(char)*30);
            strncpy(array2d[i], prevPoint, charCount);
            array2d[i][charCount]='\0';
        }
    }
    *result = array2d;

    return splitCount;
}

void freeSplit(char** result, int count){
    //printf("size:%d\n", count);
    while(--count>-1){
        /*printf("%d. %s[%x]\n", count, result[count], result[count]);*/
        free(result[count]);
    }
    /*printf("%x\n", result);*/
    free(result);
}

void parseJsonRscData(){
    int i = 0;
    ESP_LOGE(TAG, "json_data : %s", deviceRscData);
    cJSON *jsonRscData = cJSON_Parse(deviceRscData);
    
    ESP_LOGE(TAG, "json_data size : %i", cJSON_GetArraySize(jsonRscData));
    for (i = 0 ; i < cJSON_GetArraySize(jsonRscData) ; i++)
    {
        cJSON * subitem = cJSON_GetArrayItem(jsonRscData, i);
        char * jsonOid = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_OID));
        char * jsonRid = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_RID));
        char * jsonCount = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_COUNT));
        char * jsonType = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_TYPE));
        char * jsonRange = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_RANGE));
        ESP_LOGI(TAG, "oid %s", jsonOid);
        ESP_LOGI(TAG, "rid %s", jsonRid);
        ESP_LOGI(TAG, "count %s", jsonCount); 
        ESP_LOGI(TAG, "type %s", jsonType);
        ESP_LOGI(TAG, "range %s", jsonRange);
    }
}

int getNumberRangeType(char* range, char* sep){
    char *ptr = strtok(range, sep);
    int cnt = 0;
    while (ptr != NULL){
        printf("range %i : %s", cnt, ptr);
        ptr = strtok(NULL, sep);
        cnt++;
    }
    return cnt;
}

void getRscRandomValue(char* type, char* range, char* val){
    srand((unsigned int)time(NULL)); 
    int num = rand();   

    // printf("time for random : %u\n", num); 

    if(strcmp((const char *) type, "B") == 0 || strcmp((const char *) type, "b") == 0){
        if((num%2)==0){
            strcpy(val,"true");
            return;
        }else{
            strcpy(val,"false");
            return;
        }        
    }else if(strcmp((const char *) type, "S") == 0 || strcmp((const char *) type, "s") == 0){
        char** ptr = NULL;
        int cnt = split(range, ",", &ptr);
        
        for(int i=0; i<cnt; i++){
            if(num%cnt == i){
                strcpy(val,ptr[i]);
                freeSplit(ptr, cnt);
                return;
            }
        }        
    }else if(strcmp((const char *) type, "O") == 0 || strcmp((const char *) type, "o") == 0){
        strcpy(val,"Dummy Opaque");
        return;
    }else if(strcmp((const char *) type, "T") == 0 || strcmp((const char *) type, "t") == 0){
        int t = (unsigned int)time(NULL);
        itoa(t, val, 10);
        return;
    }else if(strcmp((const char *) type, "I") == 0 || strcmp((const char *) type, "i") == 0){
        char ** ptr = NULL;
        int cnt = split(range, "~", &ptr);
        if(cnt<=0){
            strcpy(val,"0");
            return;
        } 
        int fromTo[cnt];
        for(int i=0; i<cnt; i++){
            printf("ptr[%i] : %s\n", i, ptr[i]);
            fromTo[i] = atoi(ptr[i]);
            printf("fromTo[%i] : %i\n", i, fromTo[i]);
        }

        int res = num%fromTo[1] + fromTo[0];
        freeSplit(ptr, cnt);
        itoa(res, val, 10);
        printf("val : %s\n", val);
        return;
    }else if(strcmp((const char *) type, "F") == 0 || strcmp((const char *) type, "f") == 0){
        char ** ptr = NULL;
        int cnt = split(range, "~", &ptr);
        if(cnt<=0){
            strcpy(val,"0");
            return;
        } 
        float fromTo[cnt];
        for(int i=0; i<cnt; i++){
            fromTo[i] = atof(ptr[i]);
        }

        float res = num%(int)fromTo[1] + fromTo[0];
        freeSplit(ptr, cnt);
        sprintf(val, "%f", res);
        return;        
    }
}

static void time_now_us(char *now_time)
{
    struct timespec ts_now;
    char sec[12];
    sprintf(sec, "%lu", (unsigned long)time(NULL)); 

    clock_gettime(CLOCK_REALTIME, &ts_now);
    sprintf(now_time,"%s%03ld",sec,ts_now.tv_nsec/1000000);
    ESP_LOGI(TAG, "The time_now_us: ======> %s", now_time);
}

void time_sync_notification_cb(struct timeval *tv)
{
    //ESP_LOGI(TAG, "Notification of a time synchronization event");
}

void initialize_sntp()
{
    if(init_sntp) return;
    // Time init  
      ESP_LOGI(TAG, "Initializing SNTP");
      sntp_setoperatingmode(SNTP_OPMODE_POLL);
      sntp_setservername(0, "pool.ntp.org");
      sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
      sntp_set_time_sync_notification_cb(time_sync_notification_cb);
      sntp_init();
      init_sntp = true;

}


time_t obtain_time(void)
{
    ESP_LOGI(TAG, "[obtain_time] start");
    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = {};
    int retry = 0;
    const int retry_count = 200;
    char strftime_buf[64];
    while (timeinfo.tm_year < (2016 - 1900) && ++retry < retry_count)
    {
        vTaskDelay(100 / portTICK_PERIOD_MS);   // was 2000
        time(&now);
        localtime_r(&now, &timeinfo);
    }

    // Set timezone to Korea Standard Time
    setenv("TZ", "KST-9", 1);
    tzset();

    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    #ifdef DEBUG
    	ESP_LOGI(TAG, "The current date/time in South Korea is: ======> %s", strftime_buf);
    #endif
    return now;
}

/* Function to free context */
// static void adder_free_func(void *ctx)
// {
//     #ifdef DEBUG
//     	ESP_LOGI(TAG, "/adder Free Context function called");
//     #endif
//     free(ctx);
// }

/* This handler gets the present value of the accumulator */
static esp_err_t adder_get_handler(httpd_req_t *req)
{
    char resp_str[256];

#ifdef DEBUG
   	ESP_LOGI(TAG, "adder_get_handler-----> GO GO GO !!!");
    ESP_LOGI(TAG,"\n => adder_get_handler DEVICE_MANUFACTURER : %s\n",DEVICE_MANUFACTURER);
    ESP_LOGI(TAG,"\n => adder_get_handler DEVICE_MODEL : %s\n",DEVICE_MODEL);
	ESP_LOGI(TAG,"\n => adder_get_handler device_serial_str : %s\n",DEVICE_ETH_MAC);
    ESP_LOGI(TAG,"\n => adder_get_handler DEVICE_FIRMWAREVERSION : %s\n",DEVICE_FIRMWAREVERSION);
#endif

    sprintf(resp_str,"{\"manufacturer\":\"%s\",\"model\":\"%s\",\"serial\":\"%s\",\"support_type\":\"wifi\"}",DEVICE_MANUFACTURER,DEVICE_MODEL, DEVICE_ETH_MAC);
	//httpd_resp_set_status(req,HTTPD_TYPE_JSON);
	httpd_resp_set_status(req, HTTPD_200);
	httpd_resp_set_type(req, HTTPD_TYPE_JSON);
  	httpd_resp_send(req, resp_str, strlen(resp_str));
  	
    /* Respond with empty body */
    httpd_resp_send(req, NULL, 0);
    return ESP_OK;
}

/* This handler keeps accumulating data that is posted to it into a per
 * socket/session context. And returns the result.
 */
static esp_err_t adder_post_handler(httpd_req_t *req)
{
	//char resp_str[256];
	//const char* resp_str = "200";
    ESP_LOGI(TAG, "adder_post_handler-----> GO GO GO !!!");
	httpd_resp_set_status(req, HTTPD_200);
	httpd_resp_set_type(req, HTTPD_TYPE_JSON);
  	httpd_resp_send(req, "{}", 2);
	vTaskDelay(5); 
		
    char buf[256];
    char test[256];
    char* server_addr;
    char* server_port;
    char* ap_ssid;
    char* ap_password;
    char* auth_key;
    int ret, remaining = req->content_len;
	//printf("data_send : %d \n",remaining);
    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                        MIN(remaining, sizeof(buf)))) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        /* Send back the same data */
        httpd_resp_send_chunk(req, buf, ret);
        remaining -= ret;
		
        /* Log data received */
        #ifdef DEBUG
	        ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
	        ESP_LOGI(TAG, "%.*s", ret, buf);
	        ESP_LOGI(TAG, "====================================");
        #endif
        sprintf(test,"%.*s",ret,buf);
        //printf("test buff is %s\n",test);
                
        cJSON *root = cJSON_Parse(test);
        server_addr = cJSON_GetStringValue(cJSON_GetObjectItem(root,"server_addr"));
        server_port = cJSON_GetStringValue(cJSON_GetObjectItem(root,"server_port"));
        ap_ssid = cJSON_GetStringValue(cJSON_GetObjectItem(root,"ap_ssid"));
    	ap_password = cJSON_GetStringValue(cJSON_GetObjectItem(root,"ap_password"));
    	auth_key = cJSON_GetStringValue(cJSON_GetObjectItem(root,"auth_key"));

        #ifdef DEBUG
	    	printf("server_addr is : %s\n",server_addr);
	    	printf("server_port is : %s\n",server_port);
	    	printf("ap_ssid is : %s\n",ap_ssid);
	    	printf("ap_password is : %s\n",ap_password);
	    	printf("auth_key is : %s\n",auth_key);
        #endif

        flash_info.booting_mode = NORMAL_MODE;
        write_flash(&flash_info);
        
    	printf("NVS Save Starting \n");
    	nvs_open("nvs",NVS_READWRITE,&nvs);
   		nvs_set_str(nvs,"server_addr",server_addr);
   		nvs_set_str(nvs,"server_port",server_port);
   		nvs_set_str(nvs,"ap_ssid",ap_ssid);
   		nvs_set_str(nvs,"ap_password",ap_password);
   		nvs_set_str(nvs,"auth_key",auth_key);
   		nvs_commit(nvs);
    	printf("NVS Save ok \n");
    	nvs_close(nvs);
    	printf("NVS Close ok \n");
    	esp_restart();
    }
    
    return ESP_OK;
}

static esp_err_t adder_set_post_handler(httpd_req_t *req)
{
	//char resp_str[256];
	//const char* resp_str = "200";
	httpd_resp_set_status(req, HTTPD_200);
	httpd_resp_set_type(req, HTTPD_TYPE_JSON);
  	httpd_resp_send(req, "{}", 2);
	vTaskDelay(5); 
		
    char buf[256];
    char test[256];
    char* ap_ssid;
    char* ap_password;

    int ret, remaining = req->content_len;
	//printf("data_send : %d \n",remaining);
    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                        MIN(remaining, sizeof(buf)))) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        /* Send back the same data */
        httpd_resp_send_chunk(req, buf, ret);
        remaining -= ret;
		
        /* Log data received */
        #ifdef DEBUG
	        ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
	        ESP_LOGI(TAG, "%.*s", ret, buf);
	        ESP_LOGI(TAG, "====================================");
        #endif
        sprintf(test,"%.*s",ret,buf);
        //printf("test buff is %s\n",test);
                
        cJSON *root = cJSON_Parse(test);

        ap_ssid = cJSON_GetStringValue(cJSON_GetObjectItem(root,"ap_ssid"));
    	ap_password = cJSON_GetStringValue(cJSON_GetObjectItem(root,"ap_password"));

        #ifdef DEBUG
	    	printf("ap_ssid is : %s\n",ap_ssid);
	    	printf("ap_password is : %s\n",ap_password);
        #endif

        flash_info.booting_mode = NORMAL_MODE;
        write_flash(&flash_info);
        
    	printf("NVS Save Starting \n");
    	nvs_open("nvs",NVS_READWRITE,&nvs);
   		nvs_set_str(nvs,"ap_ssid",ap_ssid);
   		nvs_set_str(nvs,"ap_password",ap_password);

   		nvs_commit(nvs);

    	printf("NVS Save ok \n");
    	nvs_close(nvs);
    	printf("NVS Close ok \n");
    	esp_restart();
    }
    
    return ESP_OK;
}


#ifdef Code_Test32
static esp_err_t adder_post2_handler(httpd_req_t *req)
{

}
#endif
/* Maintain a variable which stores the number of times
 * the "/adder" URI has been visited */
// static unsigned visitors = 0;

static const httpd_uri_t adder_get = {
    .uri      = "/api/v1/wifi/info/get",
    .method   = HTTP_GET,
    .handler  = adder_get_handler,
    .user_ctx = NULL
};

static const httpd_uri_t adder_post = {
    .uri      = "/api/v1/wifi/link/start",
    .method   = HTTP_POST,
    .handler  = adder_post_handler,
    .user_ctx = NULL
};

static const httpd_uri_t adder_set_post = {
    .uri      = "/api/v1/wifi/link/set",
    .method   = HTTP_POST,
    .handler  = adder_set_post_handler,
    .user_ctx = NULL
};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server");
    
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.server_port = 18080;
	config.max_resp_headers = 20;
    
	// Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);

    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &adder_get);
        httpd_register_uri_handler(server, &adder_post);
        httpd_register_uri_handler(server, &adder_set_post);

        // httpd_register_err_handler(server, HTTPD_400_BAD_REQUEST, adder_err);
        #ifdef adder_post2
        httpd_register_uri_handler(server, &adder_post2);
        #endif
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

// static void stop_webserver(httpd_handle_t server)
// {
//     // Stop the httpd server
//     httpd_stop(server);
// }

static void event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    httpd_handle_t *server = (httpd_handle_t *) arg;
    
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STACONNECTED) 
    {
        ESP_LOGI(TAG, "WIFI_EVENT_AP_STACONNECTED");
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;   // mac[6] : ESP32 soft-AP에 연결된 스테이션의 MAC 주소
                                                                                            // aid : ESP32 soft-AP가 연결된 스테이션에 제공하는 지원
                                                                                            // is_mesh_child : 메쉬 자식을 식별하는 플래그
        ESP_LOGI(TAG, "station " MACSTR " join, AID=%d", MAC2STR(event->mac), event->aid);

        if (*server == NULL) 
        {
            *server = start_webserver();    
        }
        ap_state = 1;
    } 
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STADISCONNECTED) 
    {
        ESP_LOGI(TAG, "WIFI_EVENT_AP_STADISCONNECTED");
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;     // mac[6] : 스테이션의 MAC 주소는 ESP32 소프트 AP에 연결 해제
                                                                                                    // aid : ESP32 soft-AP가 스테이션에 제공한 지원
                                                                                                    // is_mesh_child : 메쉬 자식을 식별하는 플래그
        ESP_LOGI(TAG, "station " MACSTR " join, AID=%d", MAC2STR(event->mac), event->aid);

    }
	else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) 
	{
        ESP_LOGI(TAG, "WIFI_EVENT_STA_START");
        isForcedWifiStop = false;

        esp_wifi_connect();
       	ap_state = 0;
    } 
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_CONNECTED) 
	{  
        isForcedWifiStop = false;
        //TODO flag stop 후 중간에 타는지. 
        ESP_LOGI(TAG, "##### WIFI_EVENT_STA_CONNECTED");
    
        if(esp_compare_bufferData() == 1){
            esp_nvs_set_backupData();
        }

        // ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, &server));
        // ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_AP_STAIPASSIGNED, &event_handler, &server));
    }
	else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) 
	{               
        //TODO 
        wifi_event_sta_disconnected_t* event = (wifi_event_sta_disconnected_t*) event_data;

        ESP_LOGI(TAG, "WIFI_EVENT_STA_DISCONNECTED");
        ESP_LOGI(TAG, "Disconnected reason : %d", event->reason);
        if(isForcedWifiStop) return;

        switch(event->reason){
            case WIFI_REASON_AUTH_EXPIRE:
            case WIFI_REASON_4WAY_HANDSHAKE_TIMEOUT:
            case WIFI_REASON_AUTH_FAIL:
            case WIFI_REASON_ASSOC_EXPIRE:
            case WIFI_REASON_HANDSHAKE_TIMEOUT:
                ESP_LOGI(TAG, "STA Auth Error");
                if(esp_compare_bufferData() == 1 && s_retry_num == 0){

                    isForcedWifiStop = true;
                    s_retry_num++;

                    esp_err_t err = esp_wifi_disconnect();

                    if (err == ESP_OK){
                    ESP_ERROR_CHECK ( esp_wifi_stop ());
                    ESP_ERROR_CHECK ( esp_wifi_deinit ());
                    }

                    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
                    ESP_ERROR_CHECK(esp_wifi_init(&cfg));


                    wifi_config_t wifi_config =  esp_nvs_get_backupData();
                    if (strlen(ap_ssid_buffer) > 0)
                    {
                        
                        strcpy((char*)wifi_config.sta.ssid, (const char*)ap_ssid_buffer_b);
                        strcpy((char*)wifi_config.sta.password, (const char*)ap_password_buffer_b);
                        printf("ssid : %s \n",(char*)wifi_config.sta.ssid);
                        printf("password : %s \n",(char*)wifi_config.sta.password);
                    }
                    else
                    {

                        // wifi_config_t wifi_config = {
                        //     .sta = {
                        //         EXAMPLE_ESP_WIFI_SSID,
                        //         EXAMPLE_ESP_WIFI_PASS
                        //     },
                        // };

                        memset(wifi_ssid, 0, sizeof(wifi_ssid));
                        strcpy(wifi_ssid, WIFI_SSID);
                        memset(wifi_pass, 0, sizeof(wifi_pass));
                        strcpy(wifi_pass, WIFI_PASS);

                        wifi_config_t wifi_config;
                        memset(&wifi_config, 0, sizeof(wifi_config));
                        memcpy(wifi_config.sta.ssid, wifi_ssid, sizeof(wifi_config.sta.ssid));
                        memcpy(wifi_config.sta.password, wifi_pass, sizeof(wifi_config.sta.password));
                    }
                    ESP_LOGI(TAG, "wifi_init_sta finished.");
                    isForcedWifiStop = false;

                    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
                    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
                    ESP_ERROR_CHECK(esp_wifi_start() );

                    return;
                }else{
                    isForcedWifiStop = false;
                }
                break;
            case WIFI_REASON_NO_AP_FOUND:
                ESP_LOGI(TAG, "STA AP Not found");
                break;
            default:
                ESP_LOGI(TAG, "STA default Error");

        }
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) 
		{
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the STA");
            if (ip_get_flag)
            {
                ESP_LOGI(TAG, "WIFI_EVENT_STA_CONNECTTING~~~~~");
                   if (s_retry_num ==( EXAMPLE_ESP_MAXIMUM_RETRY-1)) 
                   {
                         esp_restart();
                   }
            }
        } 
		else 
		{
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
            
            printf("Soft AP ON \n");
            printf("Wifi Disconnect \n");
            printf("NVS format \n");
            //timer1_disable();
            
            nvs_flash_init();
            ESP_ERROR_CHECK(nvs_flash_erase());
            
            vTaskDelay(5); 
                
            printf("Soft AP MODE START \n");
            vTaskDelay(5); 
            esp_restart();
        }
        ESP_LOGI(TAG,"connect to the STA fail");
    } 
	else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) 
	{        

        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "IP_EVENT_STA_GOT_IP");
        initialize_sntp();
        obtain_time();
        time_now_us(now_time);

        
        ESP_LOGI(TAG, "IP_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        ip_get_flag = 1;
		
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
        if (*server == NULL) 
        {
            *server = start_webserver();           
            printf("Web Server Started ok.\n");
        }
		
    }
}

void wifi_init_softap(void)
{
    s_wifi_event_group = xEventGroupCreate();
    
    ESP_ERROR_CHECK(esp_netif_init()); // 기본 TCP/IP 스택을 초기화합니다.
    ESP_ERROR_CHECK(esp_event_loop_create_default()); // 기본 이벤트 루프를 만듭니다.
     esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap(); // 기본 WIFI AP를 생성 기본 WiFi 액세스 포인트 구성으로 esp_netif 개체를 생성하고 netif를 wifi에 연결하고 기본 Wi-Fi 핸들러를 등록

    ESP_ERROR_CHECK(esp_netif_dhcps_stop(ap_netif)); //DHCP 서버 중지(인터페이스 개체에서 활성화된 경우에만)

    esp_netif_ip_info_t ip_info;
    memset(&ip_info, 0, sizeof(ip_info));   
    IP4_ADDR(&ip_info.ip, 10, 10, 10, 10);
    IP4_ADDR(&ip_info.gw, 10, 10, 10, 1);
    IP4_ADDR(&ip_info.netmask, 255, 255, 255, 0);
    ESP_ERROR_CHECK(esp_netif_set_ip_info(ap_netif, &ip_info));
    ESP_ERROR_CHECK(esp_netif_dhcps_start(ap_netif));
    esp_efuse_mac_get_default(base_mac_addr);
    sprintf(DEVICE_ETH_MAC, "%02x%02x%02x%02x%02x%02x",base_mac_addr[0], base_mac_addr[1],
             base_mac_addr[2], base_mac_addr[3], base_mac_addr[4], base_mac_addr[5]);

    for(int i = 0;i<12;i++)
    {
        if(DEVICE_ETH_MAC[i]>='a'&& DEVICE_ETH_MAC[i] <='z')
        {
            DEVICE_ETH_MAC[i]= DEVICE_ETH_MAC[i]-32;
        }
    }
	
    #ifdef DEBUG
    ESP_LOGI(TAG, "Base MAC Address ==> wifi_init_softap MAC Address => %02x%02x%02x%02x%02x%02x", base_mac_addr[0], base_mac_addr[1],
             base_mac_addr[2], base_mac_addr[3], base_mac_addr[4], base_mac_addr[5]);   
    ESP_LOGI(TAG, "Device MAC Address =======================>>>>>>>> %s\n",DEVICE_ETH_MAC);
	#endif
    memcpy(flash_info.esp_mac, DEVICE_ETH_MAC, strlen(DEVICE_ETH_MAC));
   
    write_flash(&flash_info);
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, &server));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_AP_STAIPASSIGNED, &event_handler, &server));
     
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg)); //WiFi 드라이버에 대한 WiFi 할당 리소스를 초기화합니다. 이 WiFi도 WiFi 작업을 시작

    // wifi_config_t wifi_config = {
    //     .ap = {
    //         EXAMPLE_ESP_WIFI_SSID,
    //         strlen(EXAMPLE_ESP_WIFI_SSID),
    //         EXAMPLE_ESP_WIFI_CHANNEL,
    //         EXAMPLE_ESP_WIFI_PASS,
    //         EXAMPLE_MAX_STA_CONN,
    //         WIFI_AUTH_WPA_WPA2_PSK
    //     },
    // };
    wifi_config_t wifi_config = { }; // 모든 멤버를 0으로 초기화

    // 필요한 멤버를 초기화
    strcpy((char*)wifi_config.ap.ssid, EXAMPLE_ESP_WIFI_SSID);
    wifi_config.ap.ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID); // SSID의 길이
    wifi_config.ap.channel = EXAMPLE_ESP_WIFI_CHANNEL; // AP의 Wi-Fi 채널
    strcpy((char*)wifi_config.ap.password, EXAMPLE_ESP_WIFI_PASS);
    wifi_config.ap.max_connection = EXAMPLE_MAX_STA_CONN; // 동시에 연결할 수 있는 클라이언트 수
    wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK; // 인증 모드 (WPA/WPA2 PSK)

    strcpy((char *)wifi_config.sta.ssid, AP_SSID);
    strcpy((char *)wifi_config.sta.password, AP_PASS);
	
    if (strlen(AP_PASS) == 0) 
	{
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "[wifi_init_softap] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[wifi_init_softap] Minimum_Free memory: %d bytes", esp_get_minimum_free_heap_size());

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
    EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS, EXAMPLE_ESP_WIFI_CHANNEL);
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init()); // 기본 TCP/IP 스택을 초기화합니다.

    ESP_ERROR_CHECK(esp_event_loop_create_default()); // 기본 이벤트 루프를 만듭니다.
    esp_netif_create_default_wifi_sta(); // 기본 WIFI STA를 생성합니다. 초기화 오류가 발생하면 이 API가 중단됩니다.
                                         // API는 기본 WiFi 스테이션 구성으로 esp_netif 개체를 생성하고 netif를 wifi에 연결하고 기본 Wi-Fi 핸들러를 등록합니다.

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT(); 
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));//WiFi 드라이버에 대한 WiFi 할당 리소스를 초기화합니다. 이 WiFi도 WiFi 작업을 시작

    esp_wifi_get_mac(WIFI_IF_AP, eth_mac);
    //sprintf(DEVICE_ETH_MAC, "%02x%02x%02x%02x%02x%02x",eth_mac[0], eth_mac[1],
     //         eth_mac[2], eth_mac[3], eth_mac[4], eth_mac[5]);
	#ifdef DEBUG
    ESP_LOGI(TAG, "wifi_init_sta MAC Address => %02x%02x%02x%02x%02x%02x", eth_mac[0], eth_mac[1],
             eth_mac[2], eth_mac[3], eth_mac[4], eth_mac[5]);
   	#endif

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, &server));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, &server));

	wifi_config_t wifi_config = { };

	if (strlen(ap_ssid_buffer) > 0)
	{
		
		strcpy((char*)wifi_config.sta.ssid, (const char*)ap_ssid_buffer);
		strcpy((char*)wifi_config.sta.password, (const char*)ap_password_buffer);
		printf("ssid : %s \n",(char*)wifi_config.sta.ssid);
    	printf("password : %s \n",(char*)wifi_config.sta.password);
	}
    else
	{

	    // wifi_config_t wifi_config = {
	    //     .sta = {
	    //         EXAMPLE_ESP_WIFI_SSID,
	    //         EXAMPLE_ESP_WIFI_PASS
	    //     },
	    // };

        memset(wifi_ssid, 0, sizeof(wifi_ssid));
        strcpy(wifi_ssid, WIFI_SSID);
        memset(wifi_pass, 0, sizeof(wifi_pass));
        strcpy(wifi_pass, WIFI_PASS);

        wifi_config_t wifi_config;
        memset(&wifi_config, 0, sizeof(wifi_config));
        memcpy(wifi_config.sta.ssid, wifi_ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, wifi_pass, sizeof(wifi_config.sta.password));
    }
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    mdns_init();

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler));
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler));
    ESP_LOGI(TAG, "[wifi_init_sta] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[wifi_init_sta] Minimum_Free memory: %d bytes", esp_get_minimum_free_heap_size());

    vEventGroupDelete(s_wifi_event_group);

    ESP_LOGI(TAG, "[wifi_init_sta] init sntp");
    // initialize_sntp();
    // obtain_time();
    // time_now_us(now_time);
}

void UART_Command()
{
	uint8_t *data = (uint8_t *) malloc(RD_BUF_SIZE);
	NotifyParameter firmwareNotifyParam[2];
	//char* ti;
	//int i = 0;
	bzero(data, RD_BUF_SIZE);
	int len = uart_read_bytes(UART_NUM_0, data, 2, 20 / portTICK_RATE_MS);
	//Write data back to UART
	uart_write_bytes(UART_NUM_0, (const char*) data, len);
	
	
  if(strcmp((const char *) data,"a") == 0)
	{
		printf(": Soft AP ON\r\n");
		if(damda_IsMQTTConnected() == 1)damdaMQTTDisConnect();
        ESP_ERROR_CHECK(esp_netif_init());
        //ESP_ERROR_CHECK(esp_event_loop_create_default());
         esp_netif_t *ap_netif = esp_netif_create_default_wifi_ap();
        
        ESP_ERROR_CHECK(esp_netif_dhcps_stop(ap_netif));
        
        esp_netif_ip_info_t ip_info;
        memset(&ip_info, 0, sizeof(ip_info));   
        IP4_ADDR(&ip_info.ip, 10, 10, 10, 10);
        IP4_ADDR(&ip_info.gw, 10, 10, 10, 1);
        IP4_ADDR(&ip_info.netmask, 255, 255, 255, 0);
        ESP_ERROR_CHECK(esp_netif_set_ip_info(ap_netif, &ip_info));
        ESP_ERROR_CHECK(esp_netif_dhcps_start(ap_netif));
        
        ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, &server));
        ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_AP_STAIPASSIGNED, &event_handler, &server));
         
        wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
        
        // wifi_config_t wifi_config = {
        // .ap = {
        //         .ssid = EXAMPLE_ESP_WIFI_SSID,      // AP의 SSID (Wi-Fi 네트워크 이름)
        //         .ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID), // SSID의 길이
        //         .channel = EXAMPLE_ESP_WIFI_CHANNEL, // AP의 Wi-Fi 채널
        //         .password = EXAMPLE_ESP_WIFI_PASS,   // AP의 암호 (Wi-Fi 비밀번호)
        //         .max_connection = EXAMPLE_MAX_STA_CONN, // 동시에 연결할 수 있는 클라이언트 수
        //         .authmode = WIFI_AUTH_WPA_WPA2_PSK    // 인증 모드 (WPA/WPA2 PSK)
        //     }
        // };

        wifi_config_t wifi_config = { }; // 모든 멤버를 0으로 초기화

        // 필요한 멤버를 초기화
        strncpy((char*)wifi_config.ap.ssid, EXAMPLE_ESP_WIFI_SSID, sizeof(wifi_config.ap.ssid));
        wifi_config.ap.ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID); // SSID의 길이
        wifi_config.ap.channel = EXAMPLE_ESP_WIFI_CHANNEL; // AP의 Wi-Fi 채널
        strncpy((char*)wifi_config.ap.password, EXAMPLE_ESP_WIFI_PASS, sizeof(wifi_config.ap.password));
        wifi_config.ap.max_connection = EXAMPLE_MAX_STA_CONN; // 동시에 연결할 수 있는 클라이언트 수
        wifi_config.ap.authmode = WIFI_AUTH_WPA_WPA2_PSK; // 인증 모드 (WPA/WPA2 PSK)


        
        if (strlen(EXAMPLE_ESP_WIFI_PASS) == 0) 
        {
            wifi_config.ap.authmode = WIFI_AUTH_OPEN;
        }
        
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config) );
        ESP_ERROR_CHECK(esp_wifi_start());
        
        ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s channel:%d",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS, EXAMPLE_ESP_WIFI_CHANNEL);

		
	}
	else if(strcmp((const char *) data,"b") == 0){
		printf(": Soft AP OFF\r\n");
		if(damda_IsMQTTConnected() == 1)damdaMQTTDisConnect();	

        s_wifi_event_group = xEventGroupCreate();
        
        ESP_ERROR_CHECK(esp_netif_init());
        
        ESP_ERROR_CHECK(esp_event_loop_create_default());
        esp_netif_create_default_wifi_sta();
        
        wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_wifi_init(&cfg));
        
    
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, &server));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, &server));

	wifi_config_t wifi_config = { };
	if (strlen(ap_ssid_buffer) > 0)
	{
		
		strcpy((char*)wifi_config.sta.ssid, (const char*)ap_ssid_buffer);
		strcpy((char*)wifi_config.sta.password, (const char*)ap_password_buffer);
		printf("ssid : %s \n",(char*)wifi_config.sta.ssid);
    	printf("password : %s \n",(char*)wifi_config.sta.password);
	}
    else
	{

	    // wifi_config_t wifi_config = {
	    //     .sta = {
	    //         EXAMPLE_ESP_WIFI_SSID,
	    //         EXAMPLE_ESP_WIFI_PASS
	    //     },
	    // };

        memset(wifi_ssid, 0, sizeof(wifi_ssid));
        strcpy(wifi_ssid, EXAMPLE_ESP_WIFI_SSID);
        memset(wifi_pass, 0, sizeof(wifi_pass));
        strcpy(wifi_pass, EXAMPLE_ESP_WIFI_PASS);

        wifi_config_t wifi_config;
        memset(&wifi_config, 0, sizeof(wifi_config));
        memcpy(wifi_config.sta.ssid, wifi_ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, wifi_pass, sizeof(wifi_config.sta.password));
    }
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
        ESP_ERROR_CHECK(esp_wifi_start() );
        
        ESP_LOGI(TAG, "wifi_init_sta finished.");
        
        /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
         * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
        EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                pdFALSE,
                pdFALSE,
                portMAX_DELAY);
        
        /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
         * happened. */
        if (bits & WIFI_CONNECTED_BIT) {
            ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                     EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
        } else if (bits & WIFI_FAIL_BIT) {
            ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                     EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
        } else {
            ESP_LOGE(TAG, "UNEXPECTED EVENT");
        }
        
#ifdef Code_Test32

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
#else
    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler));
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler));

#endif    
    vEventGroupDelete(s_wifi_event_group);		
		
	}
	else if(strcmp((const char *) data,"c") == 0){
        if(ap_state == 0)
		{

			if(damda_IsMQTTConnected() == 1)damdaMQTTDisConnect();
			
			printf(": Mqtt connect \r\n");
			printf("MQTT : %d",damda_IsMQTTConnected());
			damdaSetDeviceInfo(DEVICE_MANUFACTURER, DEVICE_MODEL, DEVICE_ETH_MAC,flash_info.ver_string, DEVICE_TYPE);
			damdaMQTTConnect((const char*)server_addr_buffer, atoi(server_port_buffer),(const char*)auth_key_buffer);
            
            obtain_time();
			test_init();

            printf("Mqtt Callback maked \n");
		}
		else
		{
			printf("wifi not connected \n");
		}
		printf("MQTT : %d",damda_IsMQTTConnected());
        
        ESP_LOGI(TAG, "[damdaMQTTDisConnect] Free memory: %d bytes", esp_get_free_heap_size());
	
	}
	else if(strcmp((const char *) data,"d") == 0)
	{
		printf(": Register Device \r\n");
		damda_Req_RegisterDevice();
	}
    else if(strcmp((const char *) data,"e") == 0)
    {
		printf(": Update Device Information\r\n");
		damda_Req_UpdateDeviceInfo();
	}
	else if(strcmp((const char *) data,"f") == 0)
    {
		printf(": Timesync ON\r\n");
		damda_Req_ServerTimeInfo();
	}
	else if(strcmp((const char *) data,"g") == 0)
    {
		printf(": Report periodic information(notify)\r\n");
		//damdaMQTTDisConnect();
		ucnotify_flag = 1;
	}
	else if(strcmp((const char *) data,"h") == 0)
    {
		printf(": Report periodic information(notify) STOP\r\n");
		ucnotify_flag = 0;
	}
	else if(strcmp((const char *) data,"i") == 0)
	{
		printf(": Report event notification(notify)\r\n");
        ESP_LOGI(TAG, "[Report event notification] Free memory: %d bytes", esp_get_free_heap_size());
		
		obtain_time();
        time_now_us(now_time);

#ifdef SHOU
        cJSON *jsonRscData = cJSON_Parse(deviceRscData);
        int rscDataCnt = cJSON_GetArraySize(jsonRscData);
        // int noti_cnt = 0;
        int noti_index=0;
        
        printf("rscDataCnt : %i\n", rscDataCnt);
        
        NotifyParameter notifyParams[NOTI_PARAMS_TOTAL];
        char strUri[NOTI_PARAMS_TOTAL][30];
        char strVal[NOTI_PARAMS_TOTAL][50];

        for( int jArr_index=0;jArr_index<rscDataCnt;jArr_index++)
        {
            // char resUri[30];
            cJSON * subitem = cJSON_GetArrayItem(jsonRscData, jArr_index);
            char * jsonOid = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_OID));
            char * jsonRid = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_RID));
            char * jsonCount = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_COUNT));
            char * jsonType = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_TYPE));
            char * jsonRange = cJSON_GetStringValue(cJSON_GetObjectItem(subitem, RSC_RANGE));
            
            int instanceCnt = atoi(jsonCount);
            for(int i=0;i<instanceCnt;i++){
                // char bufStr[30] = {0};
                sprintf(strUri[noti_index], "/%s/0/%s", jsonOid, jsonRid);
                notifyParams[noti_index].resourceUri = strUri[noti_index];
                getRscRandomValue(jsonType, jsonRange, strVal[noti_index]);
                notifyParams[noti_index].stringValue = strVal[noti_index];
                noti_index++;
                
            }
        
        }
        printf("NOTI_PARAMS_TOTAL : %d, noti_index : %d\n", NOTI_PARAMS_TOTAL, noti_index);
        printf("%s, %d\n", __FUNCTION__, __LINE__);
        damda_Notify_DeviceStatus("2", notifyParams, noti_index);

#endif
	}
	
	else if(strcmp((const char *) data,"j") == 0){
		printf(": Unregister Device\r\n");
		// damda_Req_DeregisterDevice();
        // it makes db error
	}
	else if(strcmp((const char *) data,"k") == 0){
		printf(": Firmware Download init\r\n");
		        obtain_time();
        time_now_us(now_time);
		damda_Req_UpdateDeviceInfo();
		firmwareNotifyParam[0].resourceUri = "/5/0/3";
		firmwareNotifyParam[0].stringValue = "0";
		firmwareNotifyParam[0].time = now_time;
		firmwareNotifyParam[1].resourceUri = "/5/0/5";
		firmwareNotifyParam[1].stringValue = "0";
		firmwareNotifyParam[1].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
		damda_Notify_DeviceStatus("2",firmwareNotifyParam,2);// /5/0/3 /5/0/5 and string Value 0,0 send (download init)
		firmwareNotifyParam[0].resourceUri = "/5/0/3";
		firmwareNotifyParam[0].stringValue="1";
		firmwareNotifyParam[0].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
		damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/3 and string Value 1 send (downloading)
	}
	else if(strcmp((const char *) data,"l") == 0){
		printf(": Firmware Download\r\n");
        obtain_time();
        time_now_us(now_time);
        firmwareNotifyParam[0].resourceUri = "/5/0/3";
        firmwareNotifyParam[0].stringValue="2";
		firmwareNotifyParam[0].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
		damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/3 and string Value 1 send (downloading)
	}
	else if(strcmp((const char *) data,"m") == 0){
		printf(": Firmware info update\r\n");
		        obtain_time();
        time_now_us(now_time);
	    firmwareNotifyParam[0].resourceUri = "/5/0/3";
        firmwareNotifyParam[0].stringValue="3";
        firmwareNotifyParam[0].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
        damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/3 and string Value 3 send (Updating)
	}
	else if(strcmp((const char *) data,"n") == 0){
		printf(": Firmware update\r\n");
		        obtain_time();
        time_now_us(now_time);
        firmwareNotifyParam[0].resourceUri = "/5/0/3";
        firmwareNotifyParam[0].stringValue="0";
        firmwareNotifyParam[0].time = now_time;
		firmwareNotifyParam[1].resourceUri = "/5/0/5";
		firmwareNotifyParam[1].stringValue="1";
		firmwareNotifyParam[1].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
		damda_Notify_DeviceStatus("2",firmwareNotifyParam,2);// /5/0/5 and string Value 1 send (Successing)
		read_flash(&flash_info);
        strcpy(DEVICE_FIRMWAREVERSION,flash_info.ver_string);
	}


    else if(strcmp((const char *) data,"o") == 0)
	{
		printf(": EEPROM format\r\n");
		
		printf("Soft AP ON \n");
		printf("Wifi Disconnect \n");
		printf("NVS format \n");
		//timer1_disable();
		
		nvs_flash_init();
		ESP_ERROR_CHECK(nvs_flash_erase());
		
		vTaskDelay(5); 
			
		printf("Soft AP MODE START \n");
		vTaskDelay(5); 
 		esp_restart();
	}
    // else if(strcmp((const char *) data,"q") == 0)  // Delete device 
    // {
	// 	printf(": Delete registered device\r\n");
	// 	damda_Req_DeleteDevice();
	// }
    else if(strcmp((const char *) data,"x") == 0){
        printf(": notification\r\n");
        NotifyParameter notifyParams[10];
        
                obtain_time();
        time_now_us(now_time);
    
        notifyParams[0].resourceUri = "/3406/1/3419";
        notifyParams[0].stringValue="3";
        notifyParams[0].time = now_time;
        notifyParams[1].resourceUri = "/3406/1/4627";
        notifyParams[1].stringValue="1";
        notifyParams[1].time = now_time;
       notifyParams[2].resourceUri = "/1202/1/5200";
        notifyParams[2].stringValue="OFF";
        notifyParams[2].time = now_time;
        printf("%s, %d\n", __FUNCTION__, __LINE__);
        damda_Notify_DeviceStatus("2",notifyParams,3);// /5/0/3 and string Value 3 send (Updating)
    }
    else if(strcmp((const char *) data,"y") == 0){
            printf(": notification\r\n");
            NotifyParameter notifyParams[10];
            
            obtain_time();
            time_now_us(now_time);

            notifyParams[0].resourceUri = "/3406/1/3419";
            notifyParams[0].stringValue="6";
            notifyParams[0].time = now_time;
            notifyParams[1].resourceUri = "/3406/1/4627";
            notifyParams[1].stringValue="3";
            notifyParams[1].time = now_time;
            notifyParams[2].resourceUri = "/1202/1/5200";
            notifyParams[2].stringValue="ON";
            notifyParams[2].time = now_time;
            printf("%s, %d\n", __FUNCTION__, __LINE__);
            damda_Notify_DeviceStatus("2",notifyParams,3);// /5/0/3 and string Value 3 send (Updating)
        }
	if(strcmp((const char *) data,"\r") == 0){
		message_print_flag = 1;
		printf("Command : NULL\r\n");
	}
	
	free(data);
}

void Test_Command()
{
	if(message_print_flag == 1)
	{	
		printf("\r\n");
   		printf("\r\n");
		printf("a : Soft AP ON\r\n");
		printf("b : Soft AP OFF\r\n");
		printf("c : Mqtt connect\r\n");
		printf("d : Register Device\r\n");
		printf("e : Update Device Information\r\n");
		printf("f : Timesync ON\r\n");
		printf("g : Report periodic information(notify)\r\n");
		printf("h : Report periodic information(notify) STOP\r\n");
		printf("i : Report event notification(notify)\r\n");
		// printf("j : Unregister Device\r\n"); it makes db error.
        printf("k : Firmware update\r\n");
		//printf("k : Firmware Download init\r\n");
		//printf("l : Firmware Download\r\n");
		//printf("m : Firmware info update\r\n");
		//printf("n : Firmware update\r\n");
        printf("o : EEPROM format\r\n");
        printf("p : SDK Firmware Version : %s\n",DEVICE_FIRMWAREVERSION);
        // printf("q : Delete Device - don't use this menu as possible \r\n");
		
		message_print_flag = 0;
	}
}

void customMQTTMsgReceive(ResponseMessageInfo* responseInfo)
{
	char receivedResourceUri[1000] = {0}; // 모든 요소를 0으로 초기화
	char receivedStringvalue[1000] = {0}; // 모든 요소를 0으로 초기화

    print_receiveData(responseInfo);

    #ifdef DEBUG
		printf("custom Callback sid is %s\n",responseInfo->sid);
		printf("custom Callback returnCode is %d\n",responseInfo->returnCode);
		printf("custom Callback errorCode is %d\n",responseInfo->errorCode);
		printf("custom Callback responseType is %d\n",responseInfo->responseType);
		printf("custom Callback responseArraySize is %d\n",responseInfo->responseArraySize);
	
		//printf("custom Callback FirmwareDownloadUrl is %s\n",responseInfo->resourceUri[0]);
		//printf("custom Callback FirmwareDownloadUrl is %s\n",responseInfo->stringValue[0]);
	
		printf("custom Callback sequence is %d\n",responseInfo->sequence);
		//printf("custom Callback FirmwareVersion is %s\n",responseInfo->firmwareVersion);
		//printf("custom Callback FirmwareDownloadUrl is %s\n",responseInfo->firmwareDownloadUrl);
	#endif
    /*
        #define MESSAGE_TYPE_RESPONSE 1
        #define MESSAGE_TYPE_READ 2
        #define MESSAGE_TYPE_WRITE 3
        #define MESSAGE_TYPE_EXECUTE 4
    */
	if(responseInfo->responseType == MESSAGE_TYPE_READ || responseInfo->responseType == MESSAGE_TYPE_WRITE || responseInfo->responseType == MESSAGE_TYPE_EXECUTE)
    {
		strcpy(receivedResourceUri,responseInfo->resourceUri[0]);
		strcpy(receivedStringvalue,responseInfo->stringValue[0]);
        
        
        /*
            600 (가상의 예): 장치가 현재 다른 작업을 처리하고 있어 새 요청을 처리할 수 없음을 나타냅니다. 
            이 코드는 "바쁨" 상태를 명확히 하여 서버가 나중에 다시 시도하거나 다른 조치를 취할 수 있도록 합니다.
        */
        printf("busy_flag is %d\n",busy_flag);
        if (busy_flag )
        {
         	//damda_Rsp_RemoteControl(responseInfo->responseType,responseInfo->sid,"200",responseInfo->resourceUri,responseInfo->stringValue,responseInfo->responseArraySize);
       	   printf("\ndamda_Rsp_RemoteControl =========> damda_Rsp_Client_Busy function \n");       
           damda_Rsp_Client_Busy(responseInfo->responseType,responseInfo->sid,"600",responseInfo->responseArraySize);
                            
        }
        busy_flag  = true;
        /*
            [busy_flag 설명]
            customMQTTMsgReceive() 함수는 서버에서 명령어를 받았을 때 호출 되는 부분으로 무조건 명령이 처리가 되지 않을경우에 대해서 busy_flag가 true가 되는 구조이다.
            즉 명령어를 받았기 때문에 busy_flg는  true가 되고 해당 처리가 되면 false가 되는 구조로 되어있어서 문제가 되지 않는다.
        */

    	obtain_time();
      	time_now_us(now_time);
      	time_execute = now_time;
        cJSON *jsonRscData = cJSON_Parse(deviceRscData);
        char resUri[30];
        int rscDataCnt = cJSON_GetArraySize(jsonRscData);
        int noti_index=0;
        char* led_power[1] = {"1"};

        printf("%s %d\n", __FUNCTION__, __LINE__);


        printf("responseInfo->sid : %s\n", responseInfo->sid);
        printf("responseInfo->resourceUri : %s\n", responseInfo->resourceUri[0]);
        printf("responseInfo->stringValue : %s, length : %d\n", responseInfo->stringValue[0], strlen(responseInfo->stringValue[0]));


        if(responseInfo->responseType == MESSAGE_TYPE_EXECUTE) {
			if(strcmp(receivedResourceUri, LED_POWER)==0)
            { 
		        damda_Rsp_RemoteControl(responseInfo->responseType, responseInfo->sid, "200", responseInfo->resourceUri, led_power, responseInfo->responseArraySize);	
                busy_flag  = false;
			}
		} 
        else if(responseInfo->responseType == MESSAGE_TYPE_WRITE) {
			 if (strcmp(receivedResourceUri, IMG_MESSAGE_URL) == 0) {
                // 이미지 URL 처리
                printf("수신된 이미지 URL: %s\n", receivedStringvalue);

                
                damda_Rsp_RemoteControl(responseInfo->responseType, responseInfo->sid, "200", responseInfo->resourceUri, led_power, responseInfo->responseArraySize);	
                busy_flag  = false;

                // // // URL 전송
                // //const char* url = "https://dev.devstories.com/shou/hh-2023-12-21T20:23:32.738643.png";
                // send_url_to_receiver(receivedStringvalue);
            }
		} 
	}

	if(responseInfo->responseType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
		strcpy(ota_version,responseInfo->firmwareVersion);
		strcpy(ota_url,responseInfo->firmwareDownloadUrl);
		
        ota_task_init(); 
    }

   if ((responseInfo->returnCode == 404 )&& (responseInfo->errorCode == 40443))
   {
	   printf("Not Found Device \n");
	   printf("404 && 40443 error Code \n");
	   printf("Wifi Disconnect \n");
	   printf("NVS format \n");
       
	   nvs_flash_init();
	   ESP_ERROR_CHECK(nvs_flash_erase());
		
	   vTaskDelay(5); 
			
	   printf("Soft AP MODE START \n");
	   vTaskDelay(5); 
	   esp_restart();
   }
	if(responseInfo->responseType == MESSAGE_TYPE_DELETE_SYNC || responseInfo->responseType == MESSAGE_TYPE_DELETE_ASYNC){
			
		printf("Soft AP ON \n");
		printf("Wifi Disconnect \n");
		printf("NVS format \n");
		//timer1_disable();
		
		nvs_flash_init();
		ESP_ERROR_CHECK(nvs_flash_erase());
		
		vTaskDelay(5); 
			
		printf("Soft AP MODE START \n");
		vTaskDelay(5); 
		esp_restart();
	}
	message_print_flag = 1;

    // URL 전송
    if(strlen(receivedStringvalue) > 0)  // receivedStringvalue에 내용이 있다면 URL이 수신된 것으로 간주
        send_url_to_receiver(receivedStringvalue);	
}

void send_url_to_receiver(const char* url) {
    struct sockaddr_in dest_addr;

    // mDNS를 사용하여 receiver 장치의 IP 주소 찾기
    IPAddress receiverIP = MDNS.queryHost("receiver");
    if (receiverIP.toString() == "0.0.0.0") {
        ESP_LOGI(TAG, "Receiver not found");
    } else {
        ESP_LOGI(TAG, "Receiver IP Address: %s", receiverIP.toString().c_str());
    }

    dest_addr.sin_addr.s_addr = inet_addr(receiverIP.toString().c_str()); // IP 주소 직접 설정
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(8080);
    int addr_family = AF_INET;
    int ip_protocol = IPPROTO_IP;
    char formattedUrl[1024]; // 충분한 크기의 버퍼를 준비

    int sock = socket(addr_family, SOCK_STREAM, ip_protocol);
    if (sock < 0) {
        ESP_LOGE(TAG, "Unable to create socket: errno %d", errno);
        return;
    }

    ESP_LOGI(TAG, "Socket created, connecting to %s:8080", receiverIP.toString().c_str());

    if (connect(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr)) != 0) {
        ESP_LOGE(TAG, "Socket unable to connect: errno %d", errno);
        close(sock);
        return;
    }

    ESP_LOGI(TAG, "Successfully connected");

    // URL 뒤에 줄바꿈 문자 추가
    memset(formattedUrl, 0, sizeof(formattedUrl));
    sprintf(formattedUrl, "%s\n", url);


    if (send(sock, formattedUrl, strlen(formattedUrl), 0) < 0) {
        ESP_LOGE(TAG, "Error occurred during sending: errno %d", errno);
    } else {
        ESP_LOGI(TAG, "URL sent: %s", url);
    }
    
    if (sock != -1) {
        ESP_LOGI(TAG, "Shutting down socket and restarting...");

        vTaskDelay(1000 / portTICK_PERIOD_MS);   // send를 위한 딜레이 추가
        shutdown(sock, 0);
        close(sock);
    }
}

void test_init()
{
	damda_setMQTTMsgReceiveCallback(customMQTTMsgReceive);
}

void esp_nvs_read()
{
    printf("NVS Reading Starting \n");
    nvs_open("nvs", NVS_READWRITE, &nvs);
    size_t len;

    nvs_get_str(nvs, "server_addr", NULL, &len);
    server_addr_buffer = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "server_addr", server_addr_buffer, &len);

    nvs_get_str(nvs, "server_port", NULL, &len);
    server_port_buffer = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "server_port", server_port_buffer, &len);

    nvs_get_str(nvs, "ap_ssid", NULL, &len);
    ap_ssid_buffer = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "ap_ssid", ap_ssid_buffer, &len);

    nvs_get_str(nvs, "ap_password", NULL, &len);
    ap_password_buffer = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "ap_password", ap_password_buffer, &len);

    nvs_get_str(nvs, "auth_key", NULL, &len);
    auth_key_buffer = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "auth_key", auth_key_buffer, &len);

    nvs_get_str(nvs, "ap_ssid_b", NULL, &len);
    ap_ssid_buffer_b = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "ap_ssid_b", ap_ssid_buffer_b, &len);

    nvs_get_str(nvs, "ap_password_b", NULL, &len);
    ap_password_buffer_b = (char*)malloc(len); // 형변환 추가
    nvs_get_str(nvs, "ap_password_b", ap_password_buffer_b, &len);

    nvs_close(nvs);

    printf("NVS Reading Ending \n");
}


int esp_compare_bufferData() {

    ESP_LOGI(TAG,"Compare bufferData Start\n"); 
    if((strcmp(ap_ssid_buffer, ap_ssid_buffer_b) == 0 )&& (strcmp(ap_password_buffer, ap_password_buffer_b) == 0) ) {
        return 0;
    }else{
        ESP_LOGI(TAG,"Compared values are different\n"); 
        return 1;
    }
}

//wifi 커넥트 성공 시 호출 될 함수 (nvs에 성공한 ssid,pw를 backup데이터와 비교 후 backup데이터 저장용.
void esp_nvs_set_backupData()
{
        ESP_LOGI(TAG,"NVS Backup Data Setting Start\n"); 
        ap_ssid_buffer_b = (char*)realloc(ap_ssid_buffer_b,strlen(ap_ssid_buffer)+1);
        strcpy(ap_ssid_buffer_b, ap_ssid_buffer);

        ap_password_buffer_b = (char*)realloc(ap_password_buffer_b,strlen(ap_password_buffer)+1);
        strcpy(ap_password_buffer_b,ap_password_buffer);

        ESP_LOGI(TAG," => ap_ssid_buffer : %s\n",ap_ssid_buffer);
        ESP_LOGI(TAG," => ap_ssid_buffer_b : %s\n",ap_ssid_buffer_b);
        ESP_LOGI(TAG," => ap_password_buffer : %s\n",ap_password_buffer);
        ESP_LOGI(TAG," => ap_password_buffer_b : %s\n",ap_password_buffer_b); 

        
        nvs_open("nvs",NVS_READWRITE,&nvs);
        nvs_set_str(nvs,"ap_ssid_b",ap_ssid_buffer_b);
        nvs_set_str(nvs,"ap_password_b",ap_password_buffer_b);
        nvs_commit(nvs);
        nvs_close(nvs);
        
        ESP_LOGI(TAG,"NVS Backup Data Setting Done\n");  
}

wifi_config_t esp_nvs_get_backupData()
{
    ESP_LOGI(TAG,"Get backup data from NVS\n");  

    wifi_config_t wifi_config = { };
    if(strlen(ap_ssid_buffer_b) > 0 && strlen(ap_password_buffer_b) > 0){
        ap_ssid_buffer = (char*)realloc(ap_ssid_buffer,strlen(ap_ssid_buffer_b) +1);
        ap_password_buffer = (char*)realloc(ap_password_buffer, strlen(ap_password_buffer_b) + 1);


        strcpy(ap_ssid_buffer, ap_ssid_buffer_b);
        strcpy(ap_password_buffer, ap_password_buffer_b);

        nvs_open("nvs",NVS_READWRITE,&nvs);
            nvs_set_str(nvs,"ap_ssid",ap_ssid_buffer);
            nvs_set_str(nvs,"ap_password",ap_password_buffer);
            nvs_commit(nvs);
        nvs_close(nvs);
    }else{
        nvs_open("nvs",NVS_READWRITE,&nvs);
        
        char * ap_ssid_b = esp_nvs_load_value_if_exist(nvs,"ap_ssid_b");
        char * ap_password_b = esp_nvs_load_value_if_exist(nvs, "ap_password_b");
        
        if(ap_ssid_b != NULL && ap_password_b != NULL) {

            strcpy(ap_ssid_buffer_b, ap_ssid_b);
            strcpy(ap_password_buffer_b,ap_password_b);
            strcpy(ap_ssid_buffer, ap_ssid_b);
            strcpy(ap_password_buffer,ap_password_b);


            nvs_set_str(nvs,"ap_ssid",ap_ssid_buffer);
            nvs_set_str(nvs,"ap_password",ap_password_buffer);
            nvs_commit(nvs);

            strcpy((char*)wifi_config.sta.ssid, (const char*)ap_ssid_buffer_b);
            strcpy((char*)wifi_config.sta.password,(const char*)ap_password_buffer_b);
        }
        nvs_close(nvs);
    }
    
    return wifi_config;
}
char * esp_nvs_load_value_if_exist(nvs_handle_t nvs, const char* key) { 

    ESP_LOGI(TAG,"NVS Load %s Value Starting\n",key);

    // Try to get the size of the item
    size_t len;
    if(nvs_get_str(nvs, key, NULL, &len) != ESP_OK){
        ESP_LOGI(TAG, "Failed to get size of key: %s", key);
        return NULL;
    }

    char* value = (char*)malloc(len);
    if(nvs_get_str(nvs, key, value, &len) != ESP_OK){
        ESP_LOGI(TAG, "Failed to load key: %s", key);
        return NULL;
    }
    ESP_LOGI(TAG, "NVS Load %s Value Ending", key);
    return value;
}

void OTA_upgrade_fail(void)
{
    
	NotifyParameter firmwareNotifyParam[2];
            obtain_time();
    time_now_us(now_time);
    firmwareNotifyParam[0].resourceUri = "/5/0/5";
    firmwareNotifyParam[0].stringValue="8";
    firmwareNotifyParam[0].time = now_time;
    printf("%s, %d\n", __FUNCTION__, __LINE__);
    damda_Notify_DeviceStatus("2",firmwareNotifyParam,1);// /5/0/5 and string Value 1 send (Successing)

}

// Read Measurement
float massConcentrationPm1p0;
float massConcentrationPm2p5;
float massConcentrationPm4p0;
float massConcentrationPm10p0;
float ambientHumidity;
float ambientTemperature;
float vocIndex;
float noxIndex;
SensirionI2CSen5x sen5x;
void sensor_task(void *pvParameter)
{
    uint16_t error;
    char errorMessage[256];

    while (true)
    {
        error = sen5x.readMeasuredValues(
            massConcentrationPm1p0, massConcentrationPm2p5, massConcentrationPm4p0,
            massConcentrationPm10p0, ambientHumidity, ambientTemperature, vocIndex,
            noxIndex);

        if (error) {
            printf("Error trying to execute readMeasuredValues(): ");
            errorToString(error, errorMessage, 256);
            printf("%s", errorMessage);
        } else {
            // printf("MassConcentrationPm10p0:");
            // printf("%f", massConcentrationPm10p0);
            // printf("\t");
            // printf("AmbientHumidity:");
            // if (isnan(ambientHumidity)) {
            //     printf("n/a");
            // } else {
            //     printf("%f", ambientHumidity);
            // }
            // printf("\t");
            // printf("AmbientTemperature:");
            // if (isnan(ambientTemperature)) {
            //     printf("n/a");
            // } else {
            //     printf("%f", ambientTemperature);
            // }
            // printf("\t");
            // printf("VocIndex:");
            // if (isnan(vocIndex)) {
            //     printf("n/a");
            // } else {
            //     printf("%f", vocIndex);
            // }
            // printf("\t");
            // printf("NoxIndex:");
            // if (isnan(noxIndex)) {
            //     printf("n/a");
            // } else {
            //     printf("%f", noxIndex);
            // }
        }
        vTaskDelay(10000 / portTICK_PERIOD_MS); // portTICK_RATE_MS = 1000
    }
}
void Notify_Func()
{
    if(ucnotify_flag == 1 && damda_IsMQTTConnected() == 1)
    {
        obtain_time();
        time_now_us(now_time);

        printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ SHOU noti @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        cJSON *jsonRscData = cJSON_Parse(deviceRscData);
        int rscDataCnt = cJSON_GetArraySize(jsonRscData);
        // int noti_cnt = 0;
        int noti_index=0;

        NotifyParameter notifyParams[rscDataCnt];

        memset(ambient_temp_value, 0, sizeof(ambient_temp_value));
        memset(ambient_humidity_value, 0, sizeof(ambient_humidity_value));
        memset(ambient_pm10_value, 0, sizeof(ambient_pm10_value));
        memset(ambient_voc_value, 0, sizeof(ambient_voc_value));
        memset(led_power_value, 0, sizeof(led_power_value));


        sprintf(ambient_temp_value, "%.2f", ambientTemperature);
        sprintf(ambient_humidity_value, "%.2f", ambientHumidity);
        sprintf(ambient_pm10_value, "%.2f", massConcentrationPm10p0);
        sprintf(ambient_voc_value, "%.2f", vocIndex);
        sprintf(led_power_value, "%d", 1);       


        

        printf("@ AmbientTemperature:");
        printf("%s", ambient_temp_value);
        printf("\t");

        printf("@ AmbientHumidity:");
        printf("%s", ambient_humidity_value);
        printf("\t");        

        printf("@ MassConcentrationPm10p0:");
        printf("%s", ambient_pm10_value);
        printf("\t");

        printf("VocIndex:");
        printf("%s", ambient_voc_value);
        printf("\n");

        printf("led_power_value: ");
        printf("%s\n", led_power_value);

        // char* resUri[8] = {ERROR_CODE, IMG_MESSAGE_ID, IMG_MESSAGE_URL, AMBIENT_TEMP, AMBIENT_HUMIDITY, AMBIENT_PM10, AMBIENT_VOC, LED_POWER};
        // char* strVal[8] = {error_value, img_message_id_value, img_message_url_value, ambient_temp_value, ambient_humidity_value, ambient_pm10_value, ambient_voc_value, led_power_value};

        char* resUri[6] = {ERROR_CODE,  AMBIENT_TEMP, AMBIENT_HUMIDITY, AMBIENT_PM10, AMBIENT_VOC, LED_POWER};
        char* strVal[6] = {error_value,  ambient_temp_value, ambient_humidity_value, ambient_pm10_value, ambient_voc_value, led_power_value};

        // int instanceCnt = 0;

        for (noti_index=0; noti_index<6; noti_index++)
        {
            notifyParams[noti_index].resourceUri = resUri[noti_index];
            notifyParams[noti_index].stringValue = strVal[noti_index];
            notifyParams[noti_index].time = now_time;
        }
        printf("%s, %d\n", __FUNCTION__, __LINE__);  
        damda_Notify_DeviceStatus("2", notifyParams, noti_index);
	}
}

//========================================================================================//
#define NOTI_TIME 12 /* 50초 * num */

void main_task(void *pvParameter)
{
    int task_count = 0;
    ESP_LOGI(TAG, "main_task started");
   
    while (true)
    {
        _mil_sec++;
        if(_mil_sec >= 5000) /* 10ms 단위로 여기에 넣은 숫자와 10ms를 곱해야 정확한 시간 확인이 가능하다.*/
        {
            _mil_sec = 0;
            _mil_sec_cnt++;
            task_count++;

            if (noti_mode == 0) /* 초기에는 빠른 noti를 해야 장치가 등록이 된다. 원인 : 모름(담다 정책에 대해서 아무도 알지 못함)*/
            {
                Notify_Func();
                if (_mil_sec_cnt >= NOTI_TIME) /* 50초 10번 뒤 부터는 모두 변경 */
                {
                    _mil_sec_cnt = 0;
                    noti_mode = 1;
                }
            }
            else if (noti_mode == 1)
            {
                if (_mil_sec_cnt >= NOTI_TIME) /* 50초 10번 뒤 부터는 분 뒤 부터는 */
                {
                    Notify_Func();
                    _mil_sec_cnt = 0;   
                }
            }

            //ESP_LOGI(TAG,"\n => main_task started : %d, %d,%d\n",task_count,flash_info.ota_mode_fail, mqtt_start_flag);
            if((mqtt_start_flag)&& (flash_info.ota_mode_fail == true)&&(task_count >= 20))
            {
                ESP_LOGI(TAG, "OTA_upgrade_fail start~~~");
                OTA_upgrade_fail();
                flash_info.ota_mode_fail = false;
                write_flash(&flash_info);
                mqtt_start_flag = false;
                task_count = 0;
            }
            
            if(damda_IsMQTTConnected_Error() == 1)
            {
                printf("MQTT_EVENT_ERROR  -> Soft Ap mode Change damda_IsMQTTConnected_Error2 : %d \n", damda_IsMQTTConnected_Error());
                printf("Soft AP ON \n");
                printf("Wifi Disconnect \n");
                printf("NVS format \n");
                nvs_flash_init();
                ESP_ERROR_CHECK(nvs_flash_erase());
                vTaskDelay(5);  
                printf("MQTT_EVENT_ERROR ( Connection refused error )-> Soft AP MODE START \n");
                vTaskDelay(5); 
                esp_restart();
            }
            
        }
        vTaskDelay(10 / portTICK_PERIOD_MS); // portTICK_RATE_MS = 1000
    }
    vTaskDelete(NULL);
}


/*
    print format start

*/
void print_receiveData(ResponseMessageInfo* responseInfo){

    ESP_LOGI(TAG, "[print_receiveData] sid : %s",responseInfo->sid);
    ESP_LOGI(TAG, "[print_receiveData] sequence : %d",responseInfo->sequence);
    ESP_LOGI(TAG, "[print_receiveData] returnCode : %d",responseInfo->returnCode);
    ESP_LOGI(TAG, "[print_receiveData] errorCode : %d",responseInfo->errorCode);
    ESP_LOGI(TAG, "[print_receiveData] responseType : %d",responseInfo->responseType);
    ESP_LOGI(TAG, "[print_receiveData] responseArraySize : %d",responseInfo->responseArraySize);
    if(responseInfo->firmwareDownloadUrl != NULL){
        ESP_LOGI(TAG, "[print_receiveData] firmwareDownloadUrl :%s",responseInfo->firmwareDownloadUrl);
    }
    if(responseInfo->firmwareVersion != NULL){
        ESP_LOGI(TAG, "[print_receiveData] firmwareVersion :%s",responseInfo->firmwareVersion);
    }
    

    for (int i=0;i<responseInfo->responseArraySize;i++){
        ESP_LOGI(TAG, "[print_receiveData] resourceURI[%d] : %s",i,responseInfo->resourceUri[i]);
        //ESP_LOGI(TAG, "[print_receiveData] stringValue[%d] : %s",i,responseInfo->stringValue[i]);
       //ESP_LOGI(TAG, "[print_receiveData] time[%d] : %s",i,responseInfo->time[i]);
    }

}

// The used commands use up to 48 bytes. On some Arduino's the default buffer
// space is not large enough
#define MAXBUF_REQUIREMENT 48

#if (defined(I2C_BUFFER_LENGTH) &&                 \
     (I2C_BUFFER_LENGTH >= MAXBUF_REQUIREMENT)) || \
    (defined(BUFFER_LENGTH) && BUFFER_LENGTH >= MAXBUF_REQUIREMENT)
#define USE_PRODUCT_INFO
#endif



void printModuleVersions() {
    uint16_t error;
    char errorMessage[256];

    unsigned char productName[32];
    uint8_t productNameSize = 32;

    error = sen5x.getProductName(productName, productNameSize);

    if (error) {
       printf("Error trying to execute getProductName(): ");
        errorToString(error, errorMessage, 256);
       printf("%s\n", errorMessage);
    } else {
       printf("ProductName:");
       printf("%s\n", (char*)productName);
    }

    uint8_t firmwareMajor;
    uint8_t firmwareMinor;
    bool firmwareDebug;
    uint8_t hardwareMajor;
    uint8_t hardwareMinor;
    uint8_t protocolMajor;
    uint8_t protocolMinor;

    error = sen5x.getVersion(firmwareMajor, firmwareMinor, firmwareDebug,
                             hardwareMajor, hardwareMinor, protocolMajor,
                             protocolMinor);
    if (error) {
       printf("Error trying to execute getVersion(): ");
        errorToString(error, errorMessage, 256);
       printf("%s\n", errorMessage);
    } else {
       printf("Firmware: ");
       printf("%d", firmwareMajor);
       printf(".");
       printf("%d", firmwareMinor);
       printf(", ");

       printf("Hardware: ");
       printf("%d", hardwareMajor);
       printf(".");
       printf("%d\n", hardwareMinor);
    }
}

void printSerialNumber() {
    uint16_t error;
    char errorMessage[256];
    unsigned char serialNumber[32];
    uint8_t serialNumberSize = 32;

    error = sen5x.getSerialNumber(serialNumber, serialNumberSize);
    if (error) {
        printf("Error trying to execute getSerialNumber(): ");
        errorToString(error, errorMessage, 256);
        printf("%s\n", errorMessage);
    } else {
        printf("SerialNumber:");
        printf("%s\n", (char*)serialNumber);
    }
}

void setup() {


    Wire.begin();

    sen5x.begin(Wire);

    uint16_t error;
    char errorMessage[256];
    error = sen5x.deviceReset();
    if (error) {
        printf("Error trying to execute deviceReset(): ");
        errorToString(error, errorMessage, 256);
        printf("%s\n", errorMessage);
    }

// Print SEN55 module information if i2c buffers are large enough
#ifdef USE_PRODUCT_INFO
    printSerialNumber();
    printModuleVersions();
#endif

    // set a temperature offset in degrees celsius
    // Note: supported by SEN54 and SEN55 sensors
    // By default, the temperature and humidity outputs from the sensor
    // are compensated for the modules self-heating. If the module is
    // designed into a device, the temperature compensation might need
    // to be adapted to incorporate the change in thermal coupling and
    // self-heating of other device components.
    //
    // A guide to achieve optimal performance, including references
    // to mechanical design-in examples can be found in the app note
    // “SEN5x – Temperature Compensation Instruction” at www.sensirion.com.
    // Please refer to those application notes for further information
    // on the advanced compensation settings used
    // in `setTemperatureOffsetParameters`, `setWarmStartParameter` and
    // `setRhtAccelerationMode`.
    //
    // Adjust tempOffset to account for additional temperature offsets
    // exceeding the SEN module's self heating.
    float tempOffset = 0.0;
    error = sen5x.setTemperatureOffsetSimple(tempOffset);
    if (error) {
        printf("Error trying to execute setTemperatureOffsetSimple(): ");
        errorToString(error, errorMessage, 256);
        printf("%s\n", errorMessage);
    } else {
       printf("Temperature Offset set to ");
       printf("%f", tempOffset);
       printf(" deg. Celsius (SEN54/SEN55 only\n");
    }

    // Start Measurement
    error = sen5x.startMeasurement();
    if (error) {
       printf("Error trying to execute startMeasurement(): ");
        errorToString(error, errorMessage, 256);
       printf("%s\n", errorMessage);
    }
}



/*
    print format end

*/

// Enabling C++ compile
extern "C" { void app_main(); }

void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] Minimum_Free memory: %d bytes", esp_get_minimum_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    // nvs_flash_init();
	// 	ESP_ERROR_CHECK(nvs_flash_erase());
		
	// 	vTaskDelay(5); 
			
	// 	printf("Soft AP MODE START \n");
	// 	vTaskDelay(5); 
 	// 	esp_restart();

    damda_nvs_init();
    esp_nvs_read();
   
    user_task_init();
    damda_uart_init(); 


    setup();

   // Time init  
    //   ESP_LOGI(TAG, "Initializing SNTP");
    //   sntp_setoperatingmode(SNTP_OPMODE_POLL);
    //   sntp_setservername(0, "pool.ntp.org");
    //   sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
    //   sntp_set_time_sync_notification_cb(time_sync_notification_cb);
    //   sntp_init();


    if (flash_info.booting_mode != NORMAL_MODE)
    {
        ESP_LOGI(TAG, "ESP_WIFI_MODE_AP\n");
        wifi_init_softap();        
    }
    else
    {
  		ESP_LOGI(TAG, "ESP_WIFI_MODE_STA\n");
    	wifi_init_sta();     
    }
    
   strcpy(DEVICE_FIRMWAREVERSION,flash_info.ver_string);

   strcpy(DEVICE_ETH_MAC,flash_info.esp_mac);
   
	#ifdef DEBUG
	 ESP_LOGI(TAG,"\n => app_main DEVICE_MANUFACTURER : %s\n",DEVICE_MANUFACTURER); 
	 ESP_LOGI(TAG,"\n => app_main DEVICE_MODEL : %s\n",DEVICE_MODEL); 
 
	 ESP_LOGI(TAG,"\n => app_main DEVICE_MAC : %s\n",DEVICE_ETH_MAC); 
	 ESP_LOGI(TAG,"\n => app_main DEVICE_TYPE : %s\n",DEVICE_TYPE); 

	 ESP_LOGI(TAG,"\n => app_main DEVICE_FIRMWAREVERSION : %s\n",DEVICE_FIRMWAREVERSION); 
	#endif
    
    xTaskCreatePinnedToCore(&main_task, "main_task", 1024*4, NULL, PRIORITY_MAIN, NULL, APP_CPU_NUM);
    xTaskCreatePinnedToCore(&sensor_task, "sensor_task", 1024*4, NULL, PRIORITY_SENSOR, NULL, APP_CPU_NUM);

    message_print_flag = 1;

    if (flash_info.booting_mode == NORMAL_MODE)
    {
        if(damda_IsMQTTConnected() == 1)damdaMQTTDisConnect();
        
        printf(": Mqtt connect \r\n");
        printf("MQTT : %d",damda_IsMQTTConnected());
        memset(device_type_name, 0, sizeof(device_type_name));
        strcpy(device_type_name, DEVICE_TYPE);
        damdaSetDeviceInfo(DEVICE_MANUFACTURER, DEVICE_MODEL, DEVICE_ETH_MAC, flash_info.ver_string, device_type_name);

        RECONNECTING :
        damdaMQTTConnect((const char*)server_addr_buffer, atoi(server_port_buffer),(const char*)auth_key_buffer);        
        
        obtain_time();
        test_init();

        printf("Mqtt Callback maked \n");
		
        printf("MQTT : %d",damda_IsMQTTConnected());

        int i = 0;

        while(damda_IsMQTTConnected() != 1){
            //printf("##### Mqtt Callback wait %d \n",i);
            i++;
            if(damda_IsMQTTConnected_Error() == 1){
                free(auth_key_buffer);
                nvs_open("nvs",NVS_READWRITE,&nvs);
                size_t len;
                nvs_get_str(nvs, "auth_key", NULL, &len);
                // auth_key_buffer = malloc(len);
                auth_key_buffer = static_cast<char*>(malloc(len));
                nvs_get_str(nvs, "auth_key", auth_key_buffer, &len);
            
                nvs_close(nvs);
                goto RECONNECTING;
            }
            obtain_time();
        }

        printf(": Register Device \r\n");
        damda_Req_RegisterDevice();
        
        ESP_LOGI(TAG, "[damdaMQTTDisConnect] Free memory: %d bytes", esp_get_free_heap_size());        

        ucnotify_flag = 1;
    }

}
