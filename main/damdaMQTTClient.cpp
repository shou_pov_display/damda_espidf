#include "damdaCore.h"

//int noti_index = 0;

int messageArrivedCallbackStatus;
int damdaConnectedStatus = 0;
int damdaConnectedErrorStatus = 0;
int mqtt_err_count = 0;
extern bool mqtt_start_flag;


esp_mqtt_client_handle_t client;

char device_id[80];
char subscribeTopic[200];
MSG_ARRIVED_CALLBACK_FUNCTION callbackFunc = NULL; //messageArrivedCallback

void damda_setMQTTMsgReceiveCallback(MSG_ARRIVED_CALLBACK_FUNCTION func){
	callbackFunc = func;
	messageArrivedCallbackStatus = 1;
}

void messageArrived(char* topic,int topic_len,char* data, int data_len){
	const int cTopic_len = topic_len;
	const int cData_len = data_len;
	char receivedTopic[cTopic_len+1];
	char receivedData[cData_len+1];
	char* name[10];
	char* sv[10];
	char* ti[10];
	int nameIdx = 0;
	int svIdx = 0;
	int tiIdx = 0;
	int freeIndex = 0;
	int nSize;
	int svSize;
	int tiSize;
	int urllen;
	int firmwareVersionlen;
    char downloadSid[100]; 
    char temp_firmwareversion[10]; 
    char temp_firmwareurl[100];
	strncpy(receivedTopic,topic,topic_len);
	strncpy(receivedData,data,data_len);
	receivedData[data_len] = '\0';
	receivedTopic[topic_len] = '\0';
	
	#ifdef DEBUG 
		printf("receivedTopic is %s\n",receivedTopic);
		printf("receivedData is %s\n",receivedData);
	#endif
	
	ResponseMessageInfo resMsgInfo;
	char sid[40];
	getSid(receivedData,sid);
	int seq = getSeq(receivedData);
	int returnCode = getReturnCode(receivedData);
	int errorCode = getErrorCode(receivedData);
	int messageType = getMessageType(receivedTopic,receivedData);
	int arraySize = getNArraySize(receivedData);
	char topicOprId[10];
	getTopicOprId(receivedTopic,topicOprId);
	resMsgInfo.responseArraySize = arraySize;
	resMsgInfo.sequence = seq;
	resMsgInfo.errorCode = errorCode;
	resMsgInfo.returnCode = returnCode;
	resMsgInfo.responseType = messageType;
	resMsgInfo.firmwareVersion = NULL;
	resMsgInfo.firmwareDownloadUrl = NULL;
	if(sid != NULL){
		strcpy(resMsgInfo.sid,sid);
	}
	
	if(arraySize>0){
		for(nameIdx = 0; nameIdx<arraySize;nameIdx++){
			nSize = getResourceUriLength(receivedData,nameIdx);
			// name[nameIdx] = malloc(sizeof(char)*nSize+1);
			name[nameIdx] = static_cast<char*>(malloc(sizeof(char)*nSize+1));
            getResourceUri(receivedData,nameIdx,name[nameIdx]);
			//strcpy(name[nameIdx],getResourceUri(receivedData,nameIdx));
			#ifdef DEBUG
            	printf("\nparsing resourceURI name in loop : %s",name[nameIdx]);
			#endif
			// resMsgInfo.resourceUri[nameIdx] = malloc(sizeof(char)*nSize+1);
			resMsgInfo.resourceUri[nameIdx] = static_cast<char*>(malloc(sizeof(char)*nSize+1));
			strcpy(resMsgInfo.resourceUri[nameIdx],name[nameIdx]);
			#ifdef DEBUG
            	printf("\nparsing resourceURI in loop : %s",resMsgInfo.resourceUri[nameIdx]);
			#endif
		}
		
		if(getStringValueLength(receivedData,0)!=0){	
			for(svIdx = 0; svIdx<arraySize; svIdx++){
				svSize = getStringValueLength(receivedData,svIdx);
				// sv[svIdx] = malloc(sizeof(char)*svSize+1);
				sv[svIdx] = static_cast<char*>(malloc(sizeof(char)*svSize+1));
            	getStringValue(receivedData,svIdx,sv[svIdx]);
				//strcpy(sv[svIdx], getStringValue(receivedData,svIdx));
                #ifdef DEBUG
					printf("\nparsing stringValue value in loop : %s",sv[svIdx]);
				#endif
				// resMsgInfo.stringValue[svIdx] = malloc(sizeof(char)*svSize+1);
				resMsgInfo.stringValue[svIdx] = static_cast<char*>(malloc(sizeof(char)*svSize+1));
				strcpy(resMsgInfo.stringValue[svIdx],sv[svIdx]);
				#ifdef DEBUG	
                	printf("parsing stringValue in loop : %s",resMsgInfo.stringValue[svIdx]);
				#endif
			}
		}
		
		if(getTimeLength(receivedData,0)!=0){
			for(tiIdx = 0; tiIdx<arraySize; tiIdx++){
				tiSize = getTimeLength(receivedData,tiIdx);
				// ti[tiIdx] = malloc(sizeof(char)*tiSize+1);
				ti[tiIdx] = static_cast<char*>(malloc(sizeof(char)*tiSize+1));
                getTime(receivedData,tiIdx,ti[tiIdx]);
				//strcpy(ti[tiIdx], getTime(receivedData,tiIdx));
				// resMsgInfo.time[tiIdx] = malloc(sizeof(char)*tiSize+1);
				resMsgInfo.time[tiIdx] = static_cast<char*>(malloc(sizeof(char)*tiSize+1));
				strcpy(resMsgInfo.time[tiIdx],ti[tiIdx]);
			
			}
		}
		
	}
	
	if(messageType == MESSAGE_TYPE_DELETE_SYNC){
		damda_Rsp_RemoteDelDevice(resMsgInfo.sid,"200");
		damda_Req_DeleteDevice();
	} else if(messageType == MESSAGE_TYPE_DELETE_ASYNC){
		damda_Req_DeleteDevice();
	}

	
	if(messageType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
		if(strcmp(topicOprId,"write")==0){
			strcpy(downloadSid,sid);
			damda_Rsp_RemoteControl_noEntity(MESSAGE_TYPE_WRITE,downloadSid,"200");
		}
		getStringValueByResourceUri(receivedData,"/5/0/1",temp_firmwareurl);
        printf("\n getStringValueByResourceUri => temp_firmwareurl  : %s \n",temp_firmwareurl);
		urllen = strlen(temp_firmwareurl);
		// resMsgInfo.firmwareDownloadUrl = malloc(urllen+1);
		resMsgInfo.firmwareDownloadUrl = static_cast<char*>(malloc(urllen+1));
		strcpy(resMsgInfo.firmwareDownloadUrl,temp_firmwareurl);

        getStringValueByResourceUri(receivedData,"/3/0/3",temp_firmwareversion);
        printf("\n getStringValueByResourceUri => temp_firmwareversion  : %s \n",temp_firmwareversion);
		firmwareVersionlen = strlen(temp_firmwareversion);
		// resMsgInfo.firmwareVersion = malloc(firmwareVersionlen+1);
		resMsgInfo.firmwareVersion = static_cast<char*>(malloc(firmwareVersionlen+1));
		strcpy(resMsgInfo.firmwareVersion,temp_firmwareversion);
		
	}
	
	if(messageArrivedCallbackStatus == 1){
		callbackFunc(&resMsgInfo);
	}
	
	for(freeIndex=0;freeIndex<arraySize;freeIndex++){
		free(name[freeIndex]);
		free(resMsgInfo.resourceUri[freeIndex]);
		
		if(getStringValueLength(receivedData,0)!=0){
			free(sv[freeIndex]);
			free(resMsgInfo.stringValue[freeIndex]);
		}
		
		if(getTimeLength(receivedData,0)!=0){
			
			free(ti[freeIndex]);
			free(resMsgInfo.time[freeIndex]);
		}
	}
	if(messageType == MESSAGE_TYPE_FIRMWARE_DOWNLOAD){
		free(resMsgInfo.firmwareDownloadUrl);
		free(resMsgInfo.firmwareVersion);
	}
}


static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
	
    esp_mqtt_client_handle_t client = event->client;

    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
        	damdaConnectedStatus = 1;
            printf("MQTT Connected.\n");
			
	   		esp_mqtt_client_subscribe(client, subscribeTopic, 0);
            mqtt_start_flag = true;

            break;
        case MQTT_EVENT_DISCONNECTED:
            damdaConnectedStatus = 0;
            break;

        case MQTT_EVENT_SUBSCRIBED:
            
            printf("subscribe Done\n");
            
            break;
        case MQTT_EVENT_DATA:
            messageArrived(event->topic,event->topic_len,event->data,event->data_len);
            #ifdef DEBUG
	            printf(" ==> TOPIC=%.*s\r\n", event->topic_len, event->topic);
	            printf(" ==> DATA=%.*s\r\n", event->data_len, event->data);
	            printf("DATA Arrive\n");
			#endif
            break;
            
        case MQTT_EVENT_ERROR:
                
            printf("MQTT_EVENT_ERROR\n");
                
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_ESP_TLS) 
            {
            	 printf("Last error code reported from esp-tls: 0x%x\n", event->error_handle->esp_tls_last_esp_err);
                 printf("Last tls stack error number: 0x%x\n", event->error_handle->esp_tls_stack_err);
                 damdaConnectedErrorStatus = 0;
            }
            else if (event->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED) 
            {
                 printf("Connection refused error: 0x%x\n", event->error_handle->connect_return_code);
            	 mqtt_err_count++;
                 printf("Connection refused error %d \n",mqtt_err_count );
                
				 if(mqtt_err_count  > 5)
                 {
                 	mqtt_err_count = 0;
                    damdaConnectedErrorStatus = 1;                  
                    printf("Connection refused error damdaConnectedStatus : %d \n",damdaConnectedErrorStatus );
                 }
            } 
            else 
            {
            	printf("Unknown error type: 0x%x\n", event->error_handle->error_type);
            }
            break;

        default :
        	break;
    }
    return ESP_OK;
}

int  damdaMQTTConnect(const char* host, int port,const char* password)
{
		
		char realHost[30] = "mqtts://"; 
		
		strcat(realHost,host);
		#ifdef DEBUG
			printf("Connected Host : %s\n",realHost);
		#endif

	    // const esp_mqtt_client_config_t mqtt_cfg = {
		// //.host = "test-iot.godamda.kr",
	    //     .event_handle = mqtt_event_handler,
		// 	.uri = realHost,
	    //     .client_id = device_id,//"DAEWOO-DW_RFR-90000000066",
	    //     .username = device_id,//"DAEWOO-DW_RFR-90000000066",
	    //     .password = password,//"123456789",
	    //     .port = port,
	    //     .reconnect_timeout_ms = MQTT_RECON_DEFAULT_MS,
	    //     .cert_pem = (const char *)ca_pem
	        
	    // };

		esp_mqtt_client_config_t mqtt_cfg = {};

		mqtt_cfg.event_handle = mqtt_event_handler;
		mqtt_cfg.uri = realHost;
		mqtt_cfg.client_id = device_id;
		mqtt_cfg.username = device_id;
		mqtt_cfg.password = password;
		mqtt_cfg.port = port;
		mqtt_cfg.reconnect_timeout_ms = MQTT_RECON_DEFAULT_MS;
		mqtt_cfg.cert_pem = (const char *)ca_pem;

		
        if(strlen(device_id)==0)
        {
            #ifdef DEBUG
                    printf("\nDamda Info : not enough device Information\n");
            #endif

            return -4;
        }

        if(password == NULL)
        {
            #ifdef DEBUG
                    printf("\nDamda Info : authkey not exist\n");
            #endif

            return -5;
        }
		printf("device_id %s\n",device_id);
	    sprintf(subscribeTopic,"%s/+/iot-server/%s/#",device_type,device_id);
	    printf("subscribeTopic %s\n",subscribeTopic);
	    client = esp_mqtt_client_init(&mqtt_cfg);
	    esp_mqtt_client_start(client);

        return  1 ;
}

void damdaMQTTDisConnect(){
	//esp_mqtt_client_stop(client);
	esp_mqtt_client_destroy(client);
}
/*void damdaMQTTConnect(){
	esp_mqtt_client_start(client);
}*/
int damda_IsMQTTConnected(){
	return damdaConnectedStatus;
}

int damda_IsMQTTConnected_Error(){
	return damdaConnectedErrorStatus;
}


