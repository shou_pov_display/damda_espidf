cmake_minimum_required(VERSION 3.5)

idf_component_register(
    SRCS "src/SensirionCrc.cpp"
         "src/SensirionErrors.cpp"
         "src/SensirionI2CCommunication.cpp"
         "src/SensirionI2CTxFrame.cpp"
         "src/SensirionRxFrame.cpp"
         "src/SensirionShdlcCommunication.cpp"
         "src/SensirionShdlcTxFrame.cpp"
    INCLUDE_DIRS "src"
    REQUIRES arduino-esp32
    
)

