# WiFi station example

(See the README.md file in the upper level 'examples' directory for more information about examples.)


## How to use example

### Configure the project

```
idf.py menuconfig
```

* Set WiFi SSID and WiFi Password and Maximum retry under Example Configuration Options.

### Build and Flash

Build the project and flash it to the board, then run monitor tool to view serial output:

```
idf.py -p PORT flash monitor
```

(To exit the serial monitor, type ``Ctrl-]``.)

See the Getting Started Guide for full steps to configure and use ESP-IDF to build projects.

### 함수별 주요 기능 정리 (마크다운)

#### `wifi_init_softap()`
- **기능**: 소프트 액세스 포인트(AP) 모드로 Wi-Fi를 초기화합니다. 이 모드에서 장치는 다른 Wi-Fi 장치들에게 네트워크 접속을 제공합니다.

#### `wifi_init_sta()`
- **기능**: 스테이션(STA) 모드로 Wi-Fi를 초기화합니다. 이 모드에서 장치는 기존 Wi-Fi 네트워크에 연결합니다.

#### `main_task()`
- **기능**: 주기적으로 실행되는 메인 태스크로, 주기적인 상태 업데이트 및 기타 반복 작업을 처리합니다.

#### `app_main()`
- **기능**: 애플리케이션의 메인 진입점입니다. 초기 설정, 네트워크 연결 설정 등 전반적인 프로그램 초기화를 담당합니다.

#### `damdaMQTTConnect()`
- **기능**: MQTT 서버에 연결하기 위한 함수입니다. 이를 통해 IoT 장치는 원격 서버와 통신할 수 있습니다.

#### `parseJsonRscData()`
- **기능**: JSON 형식의 데이터를 파싱하는 함수입니다. IoT 장치에서 수집된 데이터를 처리하거나 구성합니다.

#### `Notify_Func()`
- **기능**: 특정 상태 또는 이벤트에 대한 알림을 전송하는 함수입니다. 장치의 상태 변경이나 특정 이벤트 발생 시 호출됩니다.

#### `esp_nvs_read()`, `esp_nvs_set_backupData()`, `esp_nvs_get_backupData()`
- **기능**: Non-Volatile Storage(NVS)에서 데이터를 읽고 쓰는 함수들입니다. 설정값이나 상태 정보를 영구적으로 저장하는 데 사용됩니다.

#### `UART_Command()`, `Test_Command()`
- **기능**: UART(시리얼) 통신을 통해 명령을 처리하는 함수들입니다. 사용자 입력에 대한 처리 또는 디버깅 명령을 실행합니다.

#### `customMQTTMsgReceive()`
- **기능**: MQTT 메시지를 받았을 때 호출되는 콜백 함수입니다. 네트워크를 통해 받은 명령이나 데이터를 처리합니다.

#### `OTA_upgrade_fail()`
- **기능**: OTA(Over-The-Air) 펌웨어 업그레이드 실패 시 호출되는 함수입니다. 업그레이드 실패에 대한 처리를 수행합니다.



## Return Code에 대한 정의 


**`returnCode`**는 일반적으로 네트워크 프로토콜이나 API 호출에서 상태나 결과를 나타내는 표준화된 코드입니다. 이 코드는 요청이 성공적이었는지, 오류가 발생했는지, 특정한 종류의 오류가 무엇인지 등을 알려줍니다. **`damda_Rsp_Client_Busy()`**와 **`damda_Rsp_RemoteControl()`** 함수에서 **`returnCode`**는 특히 중요하며, 다음과 같은 목적으로 사용될 수 있습니다:

### **`returnCode`의 일반적인 사용:**

- **성공(200 등)**: 요청이 성공적으로 처리되었음을 나타냅니다. 예를 들어, 장치가 센서 데이터를 성공적으로 읽었거나 설정 변경을 성공적으로 적용했을 때 이 코드를 반환할 수 있습니다.
- **클라이언트 오류(400대)**: 잘못된 요청, 권한 없는 접근, 찾을 수 없는 리소스 등 클라이언트 측의 오류를 나타냅니다. 예를 들어, 잘못된 매개변수나 인증되지 않은 요청에 대해 이 범위의 코드를 반환할 수 있습니다.
- **서버 오류(500대)**: 서버 측에 문제가 있어 요청을 처리할 수 없음을 나타냅니다. 예를 들어, 서버 내부 오류나 데이터베이스 접근 문제 등이 있을 때 이 범위의 코드를 반환할 수 있습니다.

### **`damda_Rsp_Client_Busy()`와 `damda_Rsp_RemoteControl()`에서의 `returnCode`:**

이 두 함수에서 **`returnCode`**는 다음과 같은 특정 상황을 나타내는 데 사용될 수 있습니다:

1. **damda_Rsp_Client_Busy()**:
    - **`600`** (가상의 예): 장치가 현재 다른 작업을 처리하고 있어 새 요청을 처리할 수 없음을 나타냅니다. 이 코드는 "바쁨" 상태를 명확히 하여 서버가 나중에 다시 시도하거나 다른 조치를 취할 수 있도록 합니다.
    
2. **damda_Rsp_RemoteControl()**:
    - **`200`**: 요청이 성공적으로 처리되었음을 나타냅니다. 예를 들어, 장치가 센서 값을 성공적으로 읽어 서버에 전송했을 때 이 코드를 사용할 수 있습니다.
    - **`404`**: 요청된 리소스(예: 특정 센서나 설정)를 찾을 수 없음을 나타냅니다.
    - **`500`**: 장치 내부에서 오류가 발생하여 요청을 처리할 수 없음을 나타냅니다.
    

### **결론:**

**`returnCode`**는 함수의 호출 결과를 나타내며, 이 코드를 통해 성공, 실패, 또는 특정 종류의 오류 상태를 쉽게 파악하고 적절하게 대응할 수 있습니다. 각 코드의 의미와 적절한 사용법을 이해하는 것은 효과적인 에러 핸들링과 사용자 경험 개선에 중요합니다.



# return 값에 대한 정리

| ERROR NAME | HTTP Response Code | Result Code | Description |
| --- | --- | --- | --- |
| SUCCESS | 200 | 0 | 성공 |
| NOT_PROVIDE_SERVICE | 404 | 1 | 지원하지 않는 서비스 |
| INVALID_PARAM | 400 | 2 | 잘못된 요청정보 |
| DUPLICATED_CONTENTS | 400 | 3 | 중복된 요청정보 |
| SYSTEM_ERROR | 500 | 4 | 시스템 에러 |
| INVALID_OAUTH_TOKEN | 401 | 101 | 유효하지 않은 OAUTH TOKEN |
| INVALID_AUTH_TOKEN | 401 | 102 | 유효하지 않은 AUTH TOKEN |
| NOT_EXIST_USER | 401 | 103 | 존재하지 않는 사용자 정보 |
| WRONG_PASSWORD | 401 | 104 | 잘못된 PASSWORD |
| WRONG_GROUP_ID | 401 | 105 | 잘못된 GROUP ID |
| ALREADY_EXIST_USER | 401 | 106 | 이미 존재하는 가입자 정보 |
| INVALID_TEMP_TOKEN | 401 | 107 | 유효하지 않은 임시 TOKEN |
| ALREAD_REGISTED_USER_SNS | 401 | 108 | 다른 SNS계정으로 가입된 가입자 정보 |
| WITHDRAWAL_USER | 401 | 109 | 해지된 가입자 |
| INVALID_PHONE | 400 | 201 | 잘못된 PHONE NUMBER |
| ALREADY_EXIST_INVITATION | 400 | 202 | 이미 초대된 유저 |
| INVITATION_OWNSELF | 400 | 203 | 자기 자신을 초대 |
| DUPLICATED_ERR | 400 | 301 | 중복된 자동실행 정보 |
| NOT_EXIST | 400 | 302 | 존재하지 않는 자동실행 정보 |
| INVALID_PARAM | 400 | 401 | 잘못된 액션제어 실행 정보 |
| REG_FAIL | 500 | 501 | DEVICE 등록 실패 |
| UNREG_FAIL | 500 | 502 | DEVICE 해지실패 |
| ACCESS_FAIL | 500 | 503 | HURA 접속 에러 |
| BUSY | 400 | 600 | 디바이스 동시 제어시에 기기응답이 없는 경우 |
| INVALID_DEVICE_TYPE | 400 | 601 | 유효하지 않은 DEVICE TYPE |
| NOT_FOUND_DEVICE | 400 | 602 | 찾을 수 없은 DEVICE |
| ALREADY_REGISTED_DEVICE | 400 | 603 | 이미 등록된 DEVICE |
| NOT_ASSIGNED_GROUP | 400 | 701 | 소속되지 않은 GROUP ID |
| GRADE_IS NOT_MASTER | 400 | 702 | MASTER권한이 아님 |
| TARGETUSER_NOT_ASSIGNED_GROUP | 400 | 703 | 타켓유저가 소속되지 않은 GROUP ID |
| ALREADY_USED_USER_ID | 400 | 704 | 이미 사용중인 USER ID |
| ALREADY_USED_PHONE | 400 | 705 | 이미 사용중인 PHONE |
| NOMORE_AUTHINFO | 400 | 706 | 삭제시 사용할 인증 방법이 없음 |
| AVAILABLE_FOR_OAUTH_USER | 400 | 707 | SNS 가입자에게만 허용됨 |
| ALREADY_REGISTED_PROVIDER | 400 | 708 | 이미 등록된 Provider |
| ALREAD_EXIST_OAUTH_ID | 400 | 709 | 이미 등록된 OAUTH_ID |